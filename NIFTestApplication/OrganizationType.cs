﻿namespace NIFTestApplication
{
    enum OrganizationType
    {
        /// <summary>
        ///     The Norwegian Soccer Federation
        /// </summary>
        SoccerFederation,

        /// <summary>
        ///     The Norwegian Confederation of Sports
        /// </summary>
        SportsConfederation
    }
}
