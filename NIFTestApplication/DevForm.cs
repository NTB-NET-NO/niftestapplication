﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NIFTestApplication.NIFService4.DST;
using SeasonIdRequest = NIFTestApplication.NIFService4.PROD.SeasonIdRequest;
using TournamentServiceClient = NIFTestApplication.NIFService4.PROD.TournamentServiceClient;

namespace NIFTestApplication
{
    public partial class DevForm : Form
    {
        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["devpassword"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["devusername"];

        public DevForm()
        {
            InitializeComponent();
        }

        private List<Season> GetSeasons(int orgId)
        {
            if (orgId == 0)
            {
                if (txtTournamentOrgId.Text.Trim() == string.Empty)
                {
                    txtOutput.Text = @"You must set Federation Id in text box";
                    return null;
                }
            }

            var client = new TournamentService1Client();
            if (client.ClientCredentials == null)
            {
                return null;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new SeasonRequest1(orgId);
            var response = client.GetFederationSeasons(request);

            return !response.Success ? null : response.Season.ToList();
        }


        private void btnUpdateMatchResult_Click(object sender, EventArgs e)
        {
            if (txtUpdateMatchId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Match Id" + Environment.NewLine;
                return;
            }

            if (txtUpdateHomeTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }

            if (txtUpdateAwayTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }


            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;
            var matchresult = new MatchResult
            {
                HomeGoals = Convert.ToInt32(txtUpdateHomeTeamScore.Text),
                AwayGoals = Convert.ToInt32(txtUpdateAwayTeamScore.Text),
                MatchId = Convert.ToInt32(txtUpdateMatchId.Text),
                StatusCode = "R"
            };

            var request = new MatchResultRequest(matchresult);
            var response = client.UpdateMatchResult(request);

            if (response.Success)
            {
                // good!
            }
        }

        private void DevForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnGetMatchByMatchId_Click(object sender, EventArgs e)
        {
            int matchid = Convert.ToInt32(txtMatchId.Text);

            var client = new ResultTeamService1Client();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var matchIdRequest = new MatchIdRequest1(matchid);
            var response = client.GetMatchInfo(matchIdRequest);

            if (!response.Success)
            {
                return;
            }

            MatchInfo matchInfo = response.MatchInfo;

            txtOutput.Text += @"TournamentId: " + matchInfo.TournamentId
                              + @" Tournament Name: " + matchInfo.TournamentName
                              + Environment.NewLine
                              + matchInfo.Hometeam
                              + @" - "
                              + matchInfo.Awayteam
                              + @" "
                              + matchInfo.HomeGoals + @"-" + matchInfo.AwayGoals
                              + Environment.NewLine
                              + @"VenueUnitName: " + matchInfo.VenueUnitName
                              + Environment.NewLine
                              + @"VenueUnitId : " + matchInfo.VenueUnitNo
                              + @"Partial result: " + matchInfo.PartialResult
                              + Environment.NewLine
                              + @"HomeTeamId: " + matchInfo.HometeamId + Environment.NewLine
                              + @"AwayTeamId: " + matchInfo.AwayteamId + Environment.NewLine;

            // MatchId = 4888337 
            // 319861 
            // MatchId: 4788176

            if (matchInfo.HometeamId == null) return;


            var subClient = new FunctionService1Client();
            if (subClient.ClientCredentials == null)
            {
                return;
            }

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;

            var personClient = new PersonServiceClient();

            if (personClient.ClientCredentials == null)
            {
                return;
            }

            personClient.ClientCredentials.UserName.UserName = _ntbUsername;
            personClient.ClientCredentials.UserName.Password = _ntbPassword;
            var request = new FunctionByOrgRequest1
            {
                OrgId = matchInfo.HometeamId.Value
            };

            var subResponse = subClient.GetFunctionsForOrganisation(request);

            if (!subResponse.Success)
            {
                return;
            }

            var functions = subResponse.Functions;

            foreach (var function in functions)
            {
                txtOutput.Text += @"FunctionTypeId: " + function.FunctionTypeId + @" - " + function.FunctionTypeName + Environment.NewLine;
                txtOutput.Text += function.FirstName + @" " + function.LastName + Environment.NewLine;
                txtOutput.Text += @"PersonId: " + function.PersonId + @" - " + Environment.NewLine;

                var personRequest = new PersonIdRequest2
                {
                    Id = function.PersonId
                };

                var personIdResponse = personClient.GetPerson(personRequest);
                if (personIdResponse.Success)
                {
                    var personTest = personIdResponse.Person;
                    txtOutput.Text += @"PersonId: " + personTest.PersonId + Environment.NewLine;

                    txtOutput.Text += @"PhoneHome: " + personTest.HomeAddress.PhoneHome + Environment.NewLine;
                    txtOutput.Text += @"PhoneMobile: " + personTest.HomeAddress.PhoneMobile + Environment.NewLine;
                    txtOutput.Text += @"PhoneWork: " + personTest.HomeAddress.PhoneWork + Environment.NewLine;


                }

                var searchResponse = personClient.SearchPersonByOrgIdAndFunctionTypeIds(new OrgIdFunctionTypeIdsRequest
                {
                    OrgId = matchInfo.HometeamId.Value,
                    FunctionTypeIds = new int[] { function.FunctionTypeId }
                    ,
                    MaxRows = null
                });



                if (!searchResponse.Success)
                {
                    return;
                }

                var persons = searchResponse.PersonsAndFunctions;

                foreach (var person in persons)
                {
                    txtOutput.Text += @"PersonId: " + person.PersonInfo.PersonId + Environment.NewLine;
                    txtOutput.Text += @"PhoneHome: " + person.PersonInfo.HomeTelephone + Environment.NewLine;
                    txtOutput.Text += @"MobileTelephone: " + person.PersonInfo.MobileTelephone + Environment.NewLine;
                    txtOutput.Text += @"WorkTelephone: " + person.PersonInfo.WorkTelephone + Environment.NewLine;
                }


            }
        }

        private void btnGetFederations_Click(object sender, EventArgs e)
        {
            var client = new OrgServiceClient();
            var tclient = new TournamentService1Client();
            if (client.ClientCredentials == null)
            {
                return;
            }


            if (tclient.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            tclient.ClientCredentials.UserName.UserName = _ntbUsername;
            tclient.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new EmptyRequest();
            var response = client.GetFederations(request);

            if (!response.Success)
            {
                return;
            }

            List<Federation> federations = response.Federations.ToList();
            txtOutput.Text = string.Empty;

            var outputString = new StringBuilder();
            foreach (Federation federation in federations)
            {
                outputString.Append(@"OrgId: " + federation.OrgId + Environment.NewLine +
                    @"OrgName: " + federation.OrgName + Environment.NewLine +
                    @"ShortName: " + federation.ShortName + Environment.NewLine +
                    @"BranchCode: " + federation.BranchCode + Environment.NewLine +
                    @"BranchId: " + federation.BranchId + Environment.NewLine +
                    @"BranchName: " + federation.BranchName + Environment.NewLine +
                    @"Diciplines: " + federation.Disciplines + Environment.NewLine +
                    @"HasIndividualResults: " + federation.HasIndividualResults + Environment.NewLine +
                    @"HasTeamResults" + federation.HasTeamResults + Environment.NewLine +
                    Environment.NewLine);


                var trequest = new FederationDisciplineRequest1
                {
                    OrgId = federation.OrgId
                };
                var tresponse = tclient.GetFederationDisciplines(trequest);
                if (!tresponse.Success) continue;

                foreach (FederationDiscipline discipline in tresponse.FederationDiscipline)
                {
                    outputString.Append(@"ActivityName: " + discipline.ActivityName + Environment.NewLine +
                                      @"Code: " + discipline.ActivityCode + Environment.NewLine +
                                      @"Id: " + discipline.ActivityId + Environment.NewLine);

                }
            }

            txtOutput.Text += outputString;
        }

        private void btnGetTournamentsBySeasonId_Click(object sender, EventArgs e)
        {
            txtOutput.Text = string.Empty;

            if (txtTournamentSeasonId.Text == string.Empty)
            {
                txtOutput.Text = @"You must insert a seasonId" + Environment.NewLine;
                return;
            }

            int seasonId = Convert.ToInt32(txtTournamentSeasonId.Text);

            var client = new TournamentService1Client();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var seasonRequest = new SeasonIdRequest1
            {
                SeasonId = seasonId
            };

            var response = client.GetSeasonTournaments(seasonRequest);

            if (!response.Success)
            {
                return;
            }
            var tournaments = response.Tournaments.ToList();

            var sb = new StringBuilder();
            foreach (var tournament in tournaments.Where(t => t.SeasonId == seasonId))
            {
                sb.Append(@"Id: " + tournament.TournamentId + @" " + tournament.TournamentName + Environment.NewLine);
                sb.Append("Description: " + tournament.Description + Environment.NewLine);
                sb.Append("StatusCode: " + tournament.StatusCode + Environment.NewLine);
                sb.Append("TournamentAvailabilityTypeId: " + tournament.TournamentAvailabilityTypeId + Environment.NewLine);

            }

            txtOutput.Text += sb.ToString();
        }

        private void btnGetSeasons_Click(object sender, EventArgs e)
        {
            int orgId = Convert.ToInt32(txtOrgFederationId.Text);
            List<Season> seasons = GetSeasons(orgId);
            txtOutput.Text = string.Empty;

            if (seasons == null)
            {
                txtOutput.Text = @"Ingen sesong informasjon for denne organisasjonen";
                return;
            }

            var sb = new StringBuilder();

            foreach (Season season in seasons)
            {
                sb.Append(season.SeasonId + "\t" + season.SeasonName + "\t" +
                          season.FromDate + "\t" + season.ToDate + "\t" +
                          season.SeasonStatus
                          + Environment.NewLine);
                sb.Append(@"ActivityId: " + season.ActivityId + Environment.NewLine);
                sb.Append(@"ActivityName: " + season.ActivityName + Environment.NewLine);
                sb.Append(@"Description: " + season.Description + Environment.NewLine);
            }

            txtOutput.Text = sb.ToString();
        }
    }
}
