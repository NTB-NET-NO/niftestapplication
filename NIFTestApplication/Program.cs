﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace NIFTestApplication
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var prodForm = new ProdForm();
            prodForm.StartPosition = FormStartPosition.Manual;
            prodForm.DesktopLocation = new Point(Screen.PrimaryScreen.WorkingArea.X, Screen.PrimaryScreen.WorkingArea.Y/2);
            prodForm.Show();

            var devForm = new DevForm();
            prodForm.StartPosition = FormStartPosition.Manual;
            devForm.DesktopLocation = new Point(prodForm.Left + prodForm.Width + 100, prodForm.Top);
            devForm.Show();

            Application.Run();
            // Application.Run(new ProdForm());
            // Application.Run(new DevForm());
        }
    }
}
