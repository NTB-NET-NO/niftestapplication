﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NIFTestApplication
{
    /// <summary>
    ///     This type shall be used to check if we are to show results for team sport or a single sport
    /// </summary>
    enum SportType
    {
        /// <summary>
        ///     Single Sport Type
        /// </summary>
        SingleSport,

        /// <summary>
        ///     Team Sport Type
        /// </summary>
        TeamSport
    }
}
