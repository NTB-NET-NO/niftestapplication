﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication
{
    class Utilities
    {
        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private static readonly string NtbPassword = ConfigurationManager.AppSettings["password"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private static readonly string NtbUsername = ConfigurationManager.AppSettings["username"];

        public static List<Region> GetRegions()
        {
            // So first we get regions
            var regionServiceClient = new RegionServiceClient();
            if (regionServiceClient.ClientCredentials != null)
            {
                regionServiceClient.ClientCredentials.UserName.UserName = NtbUsername;
                regionServiceClient.ClientCredentials.UserName.Password = NtbPassword;
            }

            var emptyRequest = new EmptyRequest4();
            RegionsResponse regionsResponse = regionServiceClient.GetAllRegions(emptyRequest);

            if (!regionsResponse.Success)
            {
                return null;
            }

            return new List<Region>(regionsResponse.Regions);
        }
    }
}
