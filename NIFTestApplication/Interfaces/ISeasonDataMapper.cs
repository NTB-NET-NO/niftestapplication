﻿using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Interfaces
{
    interface ISeasonDataMapper
    {
        Season GetActiveSeasonBySportId(int sportId);
    }
}
