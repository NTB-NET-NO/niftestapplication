﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NIFTestApplication.Classes;

namespace NIFTestApplication.Interfaces
{
    interface ISportDataMapper
    {
        Sport GetSportByOrgId(int orgId);
    }
}
