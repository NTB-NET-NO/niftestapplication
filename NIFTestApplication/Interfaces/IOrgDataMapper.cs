﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Interfaces
{
    interface IOrgDataMapper
    {
        Org GetOrganizationBySportId(int sportId);
    }
}
