﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using NIFTestApplication.Classes;
using NIFTestApplication.Interfaces;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Repositories
{
    class SportDataMapper : IRepository<Sport>, ISportDataMapper
    {
        public int InsertOne(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Sport> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Sport> GetAll()
        {
            throw new NotImplementedException();
        }

        public Sport Get(int id)
        {
            throw new NotImplementedException();
        }

        public Sport GetSportByOrgId(int orgId)
        {
            var sport = new Sport();
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"[Service_GetSportByOrgId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@Orgid", orgId));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                
                while (sqlDataReader.Read())
                {
                    sport.Id = Convert.ToInt32(sqlDataReader["SportId"]);
                    sport.Name = sqlDataReader["Sport"].ToString();
                }

                sqlDataReader.Close();

                sqlConnection.Close();
            }
            return sport;
        }
    }
}
