﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using NIFTestApplication.Interfaces;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Repositories
{
    class SeasonDataMapper : IRepository<Season>, ISeasonDataMapper
    {
        public int InsertOne(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Season> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Season> GetAll()
        {
            throw new NotImplementedException();
        }

        public Season Get(int id)
        {
            throw new NotImplementedException();
        }

        public Season GetActiveSeasonBySportId(int sportId)
        {
            List<Season> seasons = new List<Season>();
            using (var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_GetSeasons", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                while (sqlDataReader.Read())
                {
                    var season = new Season
                    {
                        SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                        SeasonName = sqlDataReader["SeasonName"].ToString(),
                        FromDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString()),
                        ToDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString())
                    };

                    string seasonactive = sqlDataReader["SeasonActive"].ToString().ToLower();
                    season.SeasonStatus = "true";
                    if (seasonactive == "false" || seasonactive == "0")
                    {
                        season.SeasonStatus = "false";
                    }

                    seasons.Add(season);
                }

                sqlDataReader.Close();


                sqlConnection.Close();
            }
            try
            {
                return (from s in seasons where s.SeasonStatus == "true" select s).Single();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
