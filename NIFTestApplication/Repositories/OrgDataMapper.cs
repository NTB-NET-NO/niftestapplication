﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using NIFTestApplication.Interfaces;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Repositories
{
    class OrgDataMapper : IRepository<Org>, IOrgDataMapper
    {
        public int InsertOne(Org domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Org> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Org domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Org domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Org> GetAll()
        {
            throw new NotImplementedException();
        }

        public Org Get(int id)
        {
            throw new NotImplementedException();
        }

        public Org GetOrganizationBySportId(int sportId)
        {
            using (var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"[Service_GetOrganizationBySportId]", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                var organization = new Org();
                while (sqlDataReader.Read())
                {
                    organization.Id = Convert.ToInt32(sqlDataReader["OrgId"]);
                    organization.Name = sqlDataReader["OrgName"].ToString();
                }

                sqlDataReader.Close();


                sqlConnection.Close();

                return organization;
            }
        }
    }
}
