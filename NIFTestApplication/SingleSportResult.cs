﻿using System;
using System.IO;

namespace NIFTestApplication
{
    class SingleSportResult : IDisposable
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Club { get; set; }
        public string Gender { get; set; }
        public string Time { get; set; }
        public string Nationality { get; set; }
        public string ClassExerciseName { get; set; }
        
        public int ClassExerciseId { get; set; }
        public int Rank { get; set; }

        private bool _disposed;
        private Stream _resource;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
            
        }

        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_resource != null)
                        _resource.Dispose();
                    // ReSharper disable LocalizableElement
                    Console.WriteLine("Object disposed.");
                    // ReSharper restore LocalizableElement
                }

                // Indicate that the instance has been disposed.
                _resource = null;
                _disposed = true;
            }
        }
    }

}
