﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormNIF.Designer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the FormNif type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NIFTestApplication
{
    /// <summary>
    /// The form nif.
    /// </summary>
    public partial class ProdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabTournaments = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMunicipalityId = new System.Windows.Forms.TextBox();
            this.btnGetTournamentByMunicipality = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetTournamentStanding = new System.Windows.Forms.Button();
            this.btnGetTeamsInformation = new System.Windows.Forms.Button();
            this.txtTournamentId = new System.Windows.Forms.TextBox();
            this.btnGetAllTournaments2 = new System.Windows.Forms.Button();
            this.txtTournamentEndDate = new System.Windows.Forms.TextBox();
            this.txtTournamentStartDate = new System.Windows.Forms.TextBox();
            this.btn_GetTournamentByDate = new System.Windows.Forms.Button();
            this.txtTournamentSeasonId = new System.Windows.Forms.TextBox();
            this.btnGetTournamentsBySeasonId = new System.Windows.Forms.Button();
            this.txtTournamentOrgId = new System.Windows.Forms.TextBox();
            this.btn_GetAllTournaments = new System.Windows.Forms.Button();
            this.tabOrganizations = new System.Windows.Forms.TabPage();
            this.btnSearchOrgOrClub = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOrgSeasonId = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtOrgFederationId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOrgLocalCouncilId = new System.Windows.Forms.TextBox();
            this.btnGetClubs = new System.Windows.Forms.Button();
            this.btnGetFederationClassCodes = new System.Windows.Forms.Button();
            this.btnGetSeasons = new System.Windows.Forms.Button();
            this.btnGetFederationDisciplines = new System.Windows.Forms.Button();
            this.btnGetFederations = new System.Windows.Forms.Button();
            this.tabMatches = new System.Windows.Forms.TabPage();
            this.txtFunctionsOrgId = new System.Windows.Forms.TextBox();
            this.btnGetFunctions = new System.Windows.Forms.Button();
            this.txtMatchesSeasonId = new System.Windows.Forms.TextBox();
            this.btnGetTodaysMatches = new System.Windows.Forms.Button();
            this.btnGetTournamentTable = new System.Windows.Forms.Button();
            this.btnGetMatchTeams = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtMatchId = new System.Windows.Forms.TextBox();
            this.btnGetMatchByMatchId = new System.Windows.Forms.Button();
            this.btn_GetTournamentMatches = new System.Windows.Forms.Button();
            this.txtTournamentMatchId = new System.Windows.Forms.TextBox();
            this.tabEvents = new System.Windows.Forms.TabPage();
            this.txtEventsOrgId = new System.Windows.Forms.TextBox();
            this.btnGetAllEvents = new System.Windows.Forms.Button();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tabThreadTest = new System.Windows.Forms.TabPage();
            this.btnThreadTest2 = new System.Windows.Forms.Button();
            this.btnGetAllMatchesThreadTest = new System.Windows.Forms.Button();
            this.tabRegions = new System.Windows.Forms.TabPage();
            this.btnGetRegions = new System.Windows.Forms.Button();
            this.tabFunctions = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFunctions_OrgId = new System.Windows.Forms.TextBox();
            this.btnGetFunctionsByOrgId = new System.Windows.Forms.Button();
            this.tabSingleSport = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEventId = new System.Windows.Forms.TextBox();
            this.btnGetEventResults = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEventOrgId = new System.Windows.Forms.TextBox();
            this.btnGetOrganizationEvents = new System.Windows.Forms.Button();
            this.tabMatchInsert = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeleteMatchId = new System.Windows.Forms.TextBox();
            this.btnDeleteMatchResult = new System.Windows.Forms.Button();
            this.txtInsertMatchId = new System.Windows.Forms.TextBox();
            this.txtInsertAwayTeamScore = new System.Windows.Forms.TextBox();
            this.btnInsertMatchResult = new System.Windows.Forms.Button();
            this.txtInsertHomeTeamScore = new System.Windows.Forms.TextBox();
            this.txtUpdateMatchId = new System.Windows.Forms.TextBox();
            this.txtUpdateAwayTeamScore = new System.Windows.Forms.TextBox();
            this.btnUpdateMatchResult = new System.Windows.Forms.Button();
            this.txtUpdateHomeTeamScore = new System.Windows.Forms.TextBox();
            this.timerGetTodaysMatches = new System.Windows.Forms.Timer(this.components);
            this.btnGetClubInformation = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabTournaments.SuspendLayout();
            this.tabOrganizations.SuspendLayout();
            this.tabMatches.SuspendLayout();
            this.tabEvents.SuspendLayout();
            this.tabResults.SuspendLayout();
            this.tabThreadTest.SuspendLayout();
            this.tabRegions.SuspendLayout();
            this.tabFunctions.SuspendLayout();
            this.tabSingleSport.SuspendLayout();
            this.tabMatchInsert.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(13, 13);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOutput.Size = new System.Drawing.Size(677, 266);
            this.txtOutput.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabTournaments);
            this.tabControl1.Controls.Add(this.tabOrganizations);
            this.tabControl1.Controls.Add(this.tabMatches);
            this.tabControl1.Controls.Add(this.tabEvents);
            this.tabControl1.Controls.Add(this.tabResults);
            this.tabControl1.Controls.Add(this.tabThreadTest);
            this.tabControl1.Controls.Add(this.tabRegions);
            this.tabControl1.Controls.Add(this.tabFunctions);
            this.tabControl1.Controls.Add(this.tabSingleSport);
            this.tabControl1.Controls.Add(this.tabMatchInsert);
            this.tabControl1.Location = new System.Drawing.Point(13, 286);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(677, 262);
            this.tabControl1.TabIndex = 1;
            // 
            // tabTournaments
            // 
            this.tabTournaments.Controls.Add(this.btnGetClubInformation);
            this.tabTournaments.Controls.Add(this.label16);
            this.tabTournaments.Controls.Add(this.label15);
            this.tabTournaments.Controls.Add(this.label14);
            this.tabTournaments.Controls.Add(this.txtMunicipalityId);
            this.tabTournaments.Controls.Add(this.btnGetTournamentByMunicipality);
            this.tabTournaments.Controls.Add(this.label7);
            this.tabTournaments.Controls.Add(this.label6);
            this.tabTournaments.Controls.Add(this.label2);
            this.tabTournaments.Controls.Add(this.btnGetTournamentStanding);
            this.tabTournaments.Controls.Add(this.btnGetTeamsInformation);
            this.tabTournaments.Controls.Add(this.txtTournamentId);
            this.tabTournaments.Controls.Add(this.btnGetAllTournaments2);
            this.tabTournaments.Controls.Add(this.txtTournamentEndDate);
            this.tabTournaments.Controls.Add(this.txtTournamentStartDate);
            this.tabTournaments.Controls.Add(this.btn_GetTournamentByDate);
            this.tabTournaments.Controls.Add(this.txtTournamentSeasonId);
            this.tabTournaments.Controls.Add(this.btnGetTournamentsBySeasonId);
            this.tabTournaments.Controls.Add(this.txtTournamentOrgId);
            this.tabTournaments.Controls.Add(this.btn_GetAllTournaments);
            this.tabTournaments.Location = new System.Drawing.Point(4, 22);
            this.tabTournaments.Name = "tabTournaments";
            this.tabTournaments.Padding = new System.Windows.Forms.Padding(3);
            this.tabTournaments.Size = new System.Drawing.Size(669, 236);
            this.tabTournaments.TabIndex = 0;
            this.tabTournaments.Text = "Tournaments";
            this.tabTournaments.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(427, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "End Date";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(321, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Start Date";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(219, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "RegionId";
            // 
            // txtMunicipalityId
            // 
            this.txtMunicipalityId.Location = new System.Drawing.Point(218, 25);
            this.txtMunicipalityId.Name = "txtMunicipalityId";
            this.txtMunicipalityId.Size = new System.Drawing.Size(100, 20);
            this.txtMunicipalityId.TabIndex = 20;
            // 
            // btnGetTournamentByMunicipality
            // 
            this.btnGetTournamentByMunicipality.Location = new System.Drawing.Point(228, 61);
            this.btnGetTournamentByMunicipality.Name = "btnGetTournamentByMunicipality";
            this.btnGetTournamentByMunicipality.Size = new System.Drawing.Size(213, 23);
            this.btnGetTournamentByMunicipality.TabIndex = 19;
            this.btnGetTournamentByMunicipality.Text = "Get Tournaments By Municipality";
            this.btnGetTournamentByMunicipality.UseVisualStyleBackColor = true;
            this.btnGetTournamentByMunicipality.Click += new System.EventHandler(this.btnGetTournamentByMunicipality_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(110, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "OrgId";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "SeasonId";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(539, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Tournament Id";
            // 
            // btnGetTournamentStanding
            // 
            this.btnGetTournamentStanding.Location = new System.Drawing.Point(447, 90);
            this.btnGetTournamentStanding.Name = "btnGetTournamentStanding";
            this.btnGetTournamentStanding.Size = new System.Drawing.Size(139, 23);
            this.btnGetTournamentStanding.TabIndex = 15;
            this.btnGetTournamentStanding.Text = "Get Tournament Standing";
            this.btnGetTournamentStanding.UseVisualStyleBackColor = true;
            this.btnGetTournamentStanding.Click += new System.EventHandler(this.btnGetTournamentStanding_Click);
            // 
            // btnGetTeamsInformation
            // 
            this.btnGetTeamsInformation.Location = new System.Drawing.Point(9, 119);
            this.btnGetTeamsInformation.Name = "btnGetTeamsInformation";
            this.btnGetTeamsInformation.Size = new System.Drawing.Size(139, 23);
            this.btnGetTeamsInformation.TabIndex = 12;
            this.btnGetTeamsInformation.Text = "Get Teams Information";
            this.btnGetTeamsInformation.UseVisualStyleBackColor = true;
            this.btnGetTeamsInformation.Click += new System.EventHandler(this.btnGetTeamsInformation_Click);
            // 
            // txtTournamentId
            // 
            this.txtTournamentId.Location = new System.Drawing.Point(542, 25);
            this.txtTournamentId.Name = "txtTournamentId";
            this.txtTournamentId.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentId.TabIndex = 11;
            // 
            // btnGetAllTournaments2
            // 
            this.btnGetAllTournaments2.Location = new System.Drawing.Point(228, 90);
            this.btnGetAllTournaments2.Name = "btnGetAllTournaments2";
            this.btnGetAllTournaments2.Size = new System.Drawing.Size(139, 23);
            this.btnGetAllTournaments2.TabIndex = 9;
            this.btnGetAllTournaments2.Text = "GetAllTournaments";
            this.btnGetAllTournaments2.UseVisualStyleBackColor = true;
            this.btnGetAllTournaments2.Click += new System.EventHandler(this.btnGetAllTournaments2_Click);
            // 
            // txtTournamentEndDate
            // 
            this.txtTournamentEndDate.Location = new System.Drawing.Point(430, 25);
            this.txtTournamentEndDate.Name = "txtTournamentEndDate";
            this.txtTournamentEndDate.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentEndDate.TabIndex = 8;
            // 
            // txtTournamentStartDate
            // 
            this.txtTournamentStartDate.Location = new System.Drawing.Point(324, 25);
            this.txtTournamentStartDate.Name = "txtTournamentStartDate";
            this.txtTournamentStartDate.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentStartDate.TabIndex = 7;
            // 
            // btn_GetTournamentByDate
            // 
            this.btn_GetTournamentByDate.Location = new System.Drawing.Point(9, 90);
            this.btn_GetTournamentByDate.Name = "btn_GetTournamentByDate";
            this.btn_GetTournamentByDate.Size = new System.Drawing.Size(213, 23);
            this.btn_GetTournamentByDate.TabIndex = 6;
            this.btn_GetTournamentByDate.Text = "GetTournamentsByDate";
            this.btn_GetTournamentByDate.UseVisualStyleBackColor = true;
            this.btn_GetTournamentByDate.Click += new System.EventHandler(this.btn_GetTournamentByDate_Click);
            // 
            // txtTournamentSeasonId
            // 
            this.txtTournamentSeasonId.Location = new System.Drawing.Point(6, 25);
            this.txtTournamentSeasonId.Name = "txtTournamentSeasonId";
            this.txtTournamentSeasonId.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentSeasonId.TabIndex = 4;
            // 
            // btnGetTournamentsBySeasonId
            // 
            this.btnGetTournamentsBySeasonId.Location = new System.Drawing.Point(9, 61);
            this.btnGetTournamentsBySeasonId.Name = "btnGetTournamentsBySeasonId";
            this.btnGetTournamentsBySeasonId.Size = new System.Drawing.Size(213, 23);
            this.btnGetTournamentsBySeasonId.TabIndex = 2;
            this.btnGetTournamentsBySeasonId.Text = "GetTournamentsBySeasonId";
            this.btnGetTournamentsBySeasonId.UseVisualStyleBackColor = true;
            this.btnGetTournamentsBySeasonId.Click += new System.EventHandler(this.btnGetTournamentsBySeasonId_Click);
            // 
            // txtTournamentOrgId
            // 
            this.txtTournamentOrgId.Location = new System.Drawing.Point(112, 25);
            this.txtTournamentOrgId.Name = "txtTournamentOrgId";
            this.txtTournamentOrgId.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentOrgId.TabIndex = 1;
            // 
            // btn_GetAllTournaments
            // 
            this.btn_GetAllTournaments.Location = new System.Drawing.Point(447, 61);
            this.btn_GetAllTournaments.Name = "btn_GetAllTournaments";
            this.btn_GetAllTournaments.Size = new System.Drawing.Size(139, 23);
            this.btn_GetAllTournaments.TabIndex = 0;
            this.btn_GetAllTournaments.Text = "GetAllTournaments";
            this.btn_GetAllTournaments.UseVisualStyleBackColor = true;
            this.btn_GetAllTournaments.Click += new System.EventHandler(this.btn_GetAllTournaments_Click);
            // 
            // tabOrganizations
            // 
            this.tabOrganizations.Controls.Add(this.btnSearchOrgOrClub);
            this.tabOrganizations.Controls.Add(this.label13);
            this.tabOrganizations.Controls.Add(this.txtOrgSeasonId);
            this.tabOrganizations.Controls.Add(this.label12);
            this.tabOrganizations.Controls.Add(this.txtOrgFederationId);
            this.tabOrganizations.Controls.Add(this.label11);
            this.tabOrganizations.Controls.Add(this.txtOrgLocalCouncilId);
            this.tabOrganizations.Controls.Add(this.btnGetClubs);
            this.tabOrganizations.Controls.Add(this.btnGetFederationClassCodes);
            this.tabOrganizations.Controls.Add(this.btnGetSeasons);
            this.tabOrganizations.Controls.Add(this.btnGetFederationDisciplines);
            this.tabOrganizations.Controls.Add(this.btnGetFederations);
            this.tabOrganizations.Location = new System.Drawing.Point(4, 22);
            this.tabOrganizations.Name = "tabOrganizations";
            this.tabOrganizations.Padding = new System.Windows.Forms.Padding(3);
            this.tabOrganizations.Size = new System.Drawing.Size(669, 236);
            this.tabOrganizations.TabIndex = 1;
            this.tabOrganizations.Text = "Organizations";
            this.tabOrganizations.UseVisualStyleBackColor = true;
            // 
            // btnSearchOrgOrClub
            // 
            this.btnSearchOrgOrClub.Location = new System.Drawing.Point(467, 190);
            this.btnSearchOrgOrClub.Name = "btnSearchOrgOrClub";
            this.btnSearchOrgOrClub.Size = new System.Drawing.Size(196, 24);
            this.btnSearchOrgOrClub.TabIndex = 16;
            this.btnSearchOrgOrClub.Text = "Search Org / Club";
            this.btnSearchOrgOrClub.UseVisualStyleBackColor = true;
            this.btnSearchOrgOrClub.Click += new System.EventHandler(this.btnSearchOrgOrClub_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(215, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Season Id";
            // 
            // txtOrgSeasonId
            // 
            this.txtOrgSeasonId.Location = new System.Drawing.Point(218, 35);
            this.txtOrgSeasonId.Name = "txtOrgSeasonId";
            this.txtOrgSeasonId.Size = new System.Drawing.Size(100, 20);
            this.txtOrgSeasonId.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(111, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Federation Id";
            // 
            // txtOrgFederationId
            // 
            this.txtOrgFederationId.Location = new System.Drawing.Point(112, 35);
            this.txtOrgFederationId.Name = "txtOrgFederationId";
            this.txtOrgFederationId.Size = new System.Drawing.Size(100, 20);
            this.txtOrgFederationId.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Local Council Id";
            // 
            // txtOrgLocalCouncilId
            // 
            this.txtOrgLocalCouncilId.Location = new System.Drawing.Point(6, 35);
            this.txtOrgLocalCouncilId.Name = "txtOrgLocalCouncilId";
            this.txtOrgLocalCouncilId.Size = new System.Drawing.Size(100, 20);
            this.txtOrgLocalCouncilId.TabIndex = 10;
            // 
            // btnGetClubs
            // 
            this.btnGetClubs.Location = new System.Drawing.Point(467, 160);
            this.btnGetClubs.Name = "btnGetClubs";
            this.btnGetClubs.Size = new System.Drawing.Size(196, 24);
            this.btnGetClubs.TabIndex = 9;
            this.btnGetClubs.Text = "Search Team";
            this.btnGetClubs.UseVisualStyleBackColor = true;
            this.btnGetClubs.Click += new System.EventHandler(this.btnGetClubs_Click);
            // 
            // btnGetFederationClassCodes
            // 
            this.btnGetFederationClassCodes.Location = new System.Drawing.Point(249, 190);
            this.btnGetFederationClassCodes.Name = "btnGetFederationClassCodes";
            this.btnGetFederationClassCodes.Size = new System.Drawing.Size(196, 23);
            this.btnGetFederationClassCodes.TabIndex = 7;
            this.btnGetFederationClassCodes.Text = "Get Federation Class Codes";
            this.btnGetFederationClassCodes.UseVisualStyleBackColor = true;
            this.btnGetFederationClassCodes.Click += new System.EventHandler(this.btnGetFederationClassCodes_Click);
            // 
            // btnGetSeasons
            // 
            this.btnGetSeasons.Location = new System.Drawing.Point(249, 161);
            this.btnGetSeasons.Name = "btnGetSeasons";
            this.btnGetSeasons.Size = new System.Drawing.Size(196, 23);
            this.btnGetSeasons.TabIndex = 5;
            this.btnGetSeasons.Text = "GetSeasons";
            this.btnGetSeasons.UseVisualStyleBackColor = true;
            this.btnGetSeasons.Click += new System.EventHandler(this.btnGetSeasons_Click);
            // 
            // btnGetFederationDisciplines
            // 
            this.btnGetFederationDisciplines.Location = new System.Drawing.Point(249, 132);
            this.btnGetFederationDisciplines.Name = "btnGetFederationDisciplines";
            this.btnGetFederationDisciplines.Size = new System.Drawing.Size(155, 23);
            this.btnGetFederationDisciplines.TabIndex = 3;
            this.btnGetFederationDisciplines.Text = "GetFederationDisciplines";
            this.btnGetFederationDisciplines.UseVisualStyleBackColor = true;
            this.btnGetFederationDisciplines.Click += new System.EventHandler(this.btnGetFederationDisciplines_Click);
            // 
            // btnGetFederations
            // 
            this.btnGetFederations.Location = new System.Drawing.Point(467, 131);
            this.btnGetFederations.Name = "btnGetFederations";
            this.btnGetFederations.Size = new System.Drawing.Size(99, 23);
            this.btnGetFederations.TabIndex = 2;
            this.btnGetFederations.Text = "GetFederations";
            this.btnGetFederations.UseVisualStyleBackColor = true;
            this.btnGetFederations.Click += new System.EventHandler(this.btnGetFederations_Click);
            // 
            // tabMatches
            // 
            this.tabMatches.Controls.Add(this.txtFunctionsOrgId);
            this.tabMatches.Controls.Add(this.btnGetFunctions);
            this.tabMatches.Controls.Add(this.txtMatchesSeasonId);
            this.tabMatches.Controls.Add(this.btnGetTodaysMatches);
            this.tabMatches.Controls.Add(this.btnGetTournamentTable);
            this.tabMatches.Controls.Add(this.btnGetMatchTeams);
            this.tabMatches.Controls.Add(this.button1);
            this.tabMatches.Controls.Add(this.txtMatchId);
            this.tabMatches.Controls.Add(this.btnGetMatchByMatchId);
            this.tabMatches.Controls.Add(this.btn_GetTournamentMatches);
            this.tabMatches.Controls.Add(this.txtTournamentMatchId);
            this.tabMatches.Location = new System.Drawing.Point(4, 22);
            this.tabMatches.Name = "tabMatches";
            this.tabMatches.Size = new System.Drawing.Size(669, 236);
            this.tabMatches.TabIndex = 2;
            this.tabMatches.Text = "Matches";
            this.tabMatches.UseVisualStyleBackColor = true;
            // 
            // txtFunctionsOrgId
            // 
            this.txtFunctionsOrgId.Location = new System.Drawing.Point(269, 37);
            this.txtFunctionsOrgId.Name = "txtFunctionsOrgId";
            this.txtFunctionsOrgId.Size = new System.Drawing.Size(100, 20);
            this.txtFunctionsOrgId.TabIndex = 10;
            // 
            // btnGetFunctions
            // 
            this.btnGetFunctions.Location = new System.Drawing.Point(375, 35);
            this.btnGetFunctions.Name = "btnGetFunctions";
            this.btnGetFunctions.Size = new System.Drawing.Size(153, 23);
            this.btnGetFunctions.TabIndex = 9;
            this.btnGetFunctions.Text = "Get Functions";
            this.btnGetFunctions.UseVisualStyleBackColor = true;
            this.btnGetFunctions.Click += new System.EventHandler(this.btnGetFunctions_Click);
            // 
            // txtMatchesSeasonId
            // 
            this.txtMatchesSeasonId.Location = new System.Drawing.Point(4, 133);
            this.txtMatchesSeasonId.Name = "txtMatchesSeasonId";
            this.txtMatchesSeasonId.Size = new System.Drawing.Size(100, 20);
            this.txtMatchesSeasonId.TabIndex = 8;
            // 
            // btnGetTodaysMatches
            // 
            this.btnGetTodaysMatches.Location = new System.Drawing.Point(110, 131);
            this.btnGetTodaysMatches.Name = "btnGetTodaysMatches";
            this.btnGetTodaysMatches.Size = new System.Drawing.Size(153, 23);
            this.btnGetTodaysMatches.TabIndex = 7;
            this.btnGetTodaysMatches.Text = "Get Todays Matches";
            this.btnGetTodaysMatches.UseVisualStyleBackColor = true;
            this.btnGetTodaysMatches.Click += new System.EventHandler(this.btnGetTodaysMatches_Click);
            // 
            // btnGetTournamentTable
            // 
            this.btnGetTournamentTable.Location = new System.Drawing.Point(269, 3);
            this.btnGetTournamentTable.Name = "btnGetTournamentTable";
            this.btnGetTournamentTable.Size = new System.Drawing.Size(153, 23);
            this.btnGetTournamentTable.TabIndex = 6;
            this.btnGetTournamentTable.Text = "GetTournamentTable";
            this.btnGetTournamentTable.UseVisualStyleBackColor = true;
            this.btnGetTournamentTable.Click += new System.EventHandler(this.btnGetTournamentTable_Click);
            // 
            // btnGetMatchTeams
            // 
            this.btnGetMatchTeams.Location = new System.Drawing.Point(110, 92);
            this.btnGetMatchTeams.Name = "btnGetMatchTeams";
            this.btnGetMatchTeams.Size = new System.Drawing.Size(153, 23);
            this.btnGetMatchTeams.TabIndex = 5;
            this.btnGetMatchTeams.Text = "GetMatchTeams";
            this.btnGetMatchTeams.UseVisualStyleBackColor = true;
            this.btnGetMatchTeams.Click += new System.EventHandler(this.btnGetMatchTeams_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(110, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(153, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "GetMatchDetails";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtMatchId
            // 
            this.txtMatchId.Location = new System.Drawing.Point(3, 37);
            this.txtMatchId.Name = "txtMatchId";
            this.txtMatchId.Size = new System.Drawing.Size(100, 20);
            this.txtMatchId.TabIndex = 3;
            // 
            // btnGetMatchByMatchId
            // 
            this.btnGetMatchByMatchId.Location = new System.Drawing.Point(110, 34);
            this.btnGetMatchByMatchId.Name = "btnGetMatchByMatchId";
            this.btnGetMatchByMatchId.Size = new System.Drawing.Size(153, 23);
            this.btnGetMatchByMatchId.TabIndex = 2;
            this.btnGetMatchByMatchId.Text = "GetMatchInfo";
            this.btnGetMatchByMatchId.UseVisualStyleBackColor = true;
            this.btnGetMatchByMatchId.Click += new System.EventHandler(this.btnGetMatchByMatchId_Click);
            // 
            // btn_GetTournamentMatches
            // 
            this.btn_GetTournamentMatches.Location = new System.Drawing.Point(110, 4);
            this.btn_GetTournamentMatches.Name = "btn_GetTournamentMatches";
            this.btn_GetTournamentMatches.Size = new System.Drawing.Size(153, 23);
            this.btn_GetTournamentMatches.TabIndex = 1;
            this.btn_GetTournamentMatches.Text = "GetTournamentMatches";
            this.btn_GetTournamentMatches.UseVisualStyleBackColor = true;
            this.btn_GetTournamentMatches.Click += new System.EventHandler(this.btn_GetTournamentMatches_Click);
            // 
            // txtTournamentMatchId
            // 
            this.txtTournamentMatchId.Location = new System.Drawing.Point(4, 6);
            this.txtTournamentMatchId.Name = "txtTournamentMatchId";
            this.txtTournamentMatchId.Size = new System.Drawing.Size(100, 20);
            this.txtTournamentMatchId.TabIndex = 0;
            // 
            // tabEvents
            // 
            this.tabEvents.Controls.Add(this.txtEventsOrgId);
            this.tabEvents.Controls.Add(this.btnGetAllEvents);
            this.tabEvents.Location = new System.Drawing.Point(4, 22);
            this.tabEvents.Name = "tabEvents";
            this.tabEvents.Size = new System.Drawing.Size(669, 236);
            this.tabEvents.TabIndex = 3;
            this.tabEvents.Text = "Events";
            this.tabEvents.UseVisualStyleBackColor = true;
            // 
            // txtEventsOrgId
            // 
            this.txtEventsOrgId.Location = new System.Drawing.Point(3, 6);
            this.txtEventsOrgId.Name = "txtEventsOrgId";
            this.txtEventsOrgId.Size = new System.Drawing.Size(100, 20);
            this.txtEventsOrgId.TabIndex = 2;
            // 
            // btnGetAllEvents
            // 
            this.btnGetAllEvents.Location = new System.Drawing.Point(109, 6);
            this.btnGetAllEvents.Name = "btnGetAllEvents";
            this.btnGetAllEvents.Size = new System.Drawing.Size(139, 23);
            this.btnGetAllEvents.TabIndex = 1;
            this.btnGetAllEvents.Text = "GetAllEvents";
            this.btnGetAllEvents.UseVisualStyleBackColor = true;
            this.btnGetAllEvents.Click += new System.EventHandler(this.btnGetAllEvents_Click);
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.textBox3);
            this.tabResults.Controls.Add(this.button2);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Size = new System.Drawing.Size(669, 236);
            this.tabResults.TabIndex = 4;
            this.tabResults.Text = "Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(3, 23);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(109, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "GetAllTournaments";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tabThreadTest
            // 
            this.tabThreadTest.Controls.Add(this.btnThreadTest2);
            this.tabThreadTest.Controls.Add(this.btnGetAllMatchesThreadTest);
            this.tabThreadTest.Location = new System.Drawing.Point(4, 22);
            this.tabThreadTest.Name = "tabThreadTest";
            this.tabThreadTest.Size = new System.Drawing.Size(669, 236);
            this.tabThreadTest.TabIndex = 5;
            this.tabThreadTest.Text = "Thread Test";
            this.tabThreadTest.UseVisualStyleBackColor = true;
            // 
            // btnThreadTest2
            // 
            this.btnThreadTest2.Location = new System.Drawing.Point(142, 67);
            this.btnThreadTest2.Name = "btnThreadTest2";
            this.btnThreadTest2.Size = new System.Drawing.Size(404, 23);
            this.btnThreadTest2.TabIndex = 1;
            this.btnThreadTest2.Text = "Get Matches from All Tournaments in All Sports 2";
            this.btnThreadTest2.UseVisualStyleBackColor = true;
            this.btnThreadTest2.Click += new System.EventHandler(this.btnThreadTest2_Click);
            // 
            // btnGetAllMatchesThreadTest
            // 
            this.btnGetAllMatchesThreadTest.Location = new System.Drawing.Point(142, 26);
            this.btnGetAllMatchesThreadTest.Name = "btnGetAllMatchesThreadTest";
            this.btnGetAllMatchesThreadTest.Size = new System.Drawing.Size(404, 23);
            this.btnGetAllMatchesThreadTest.TabIndex = 0;
            this.btnGetAllMatchesThreadTest.Text = "Get Matches from All Tournaments in All Sports";
            this.btnGetAllMatchesThreadTest.UseVisualStyleBackColor = true;
            this.btnGetAllMatchesThreadTest.Click += new System.EventHandler(this.btnGetAllMatchesThreadTest_Click);
            // 
            // tabRegions
            // 
            this.tabRegions.Controls.Add(this.btnGetRegions);
            this.tabRegions.Location = new System.Drawing.Point(4, 22);
            this.tabRegions.Name = "tabRegions";
            this.tabRegions.Size = new System.Drawing.Size(669, 236);
            this.tabRegions.TabIndex = 6;
            this.tabRegions.Text = "Regions";
            this.tabRegions.UseVisualStyleBackColor = true;
            // 
            // btnGetRegions
            // 
            this.btnGetRegions.Location = new System.Drawing.Point(3, 3);
            this.btnGetRegions.Name = "btnGetRegions";
            this.btnGetRegions.Size = new System.Drawing.Size(88, 29);
            this.btnGetRegions.TabIndex = 1;
            this.btnGetRegions.Text = "Get Regions";
            this.btnGetRegions.UseVisualStyleBackColor = true;
            this.btnGetRegions.Click += new System.EventHandler(this.btnGetRegions_Click);
            // 
            // tabFunctions
            // 
            this.tabFunctions.Controls.Add(this.label3);
            this.tabFunctions.Controls.Add(this.txtFunctions_OrgId);
            this.tabFunctions.Controls.Add(this.btnGetFunctionsByOrgId);
            this.tabFunctions.Location = new System.Drawing.Point(4, 22);
            this.tabFunctions.Name = "tabFunctions";
            this.tabFunctions.Size = new System.Drawing.Size(669, 236);
            this.tabFunctions.TabIndex = 7;
            this.tabFunctions.Text = "Functions";
            this.tabFunctions.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "OrgId";
            // 
            // txtFunctions_OrgId
            // 
            this.txtFunctions_OrgId.Location = new System.Drawing.Point(3, 26);
            this.txtFunctions_OrgId.Name = "txtFunctions_OrgId";
            this.txtFunctions_OrgId.Size = new System.Drawing.Size(100, 20);
            this.txtFunctions_OrgId.TabIndex = 6;
            // 
            // btnGetFunctionsByOrgId
            // 
            this.btnGetFunctionsByOrgId.Location = new System.Drawing.Point(109, 24);
            this.btnGetFunctionsByOrgId.Name = "btnGetFunctionsByOrgId";
            this.btnGetFunctionsByOrgId.Size = new System.Drawing.Size(155, 23);
            this.btnGetFunctionsByOrgId.TabIndex = 5;
            this.btnGetFunctionsByOrgId.Text = "Get Federation Functions";
            this.btnGetFunctionsByOrgId.UseVisualStyleBackColor = true;
            this.btnGetFunctionsByOrgId.Click += new System.EventHandler(this.btnGetFunctionsByOrgId_Click);
            // 
            // tabSingleSport
            // 
            this.tabSingleSport.Controls.Add(this.label5);
            this.tabSingleSport.Controls.Add(this.txtEventId);
            this.tabSingleSport.Controls.Add(this.btnGetEventResults);
            this.tabSingleSport.Controls.Add(this.label4);
            this.tabSingleSport.Controls.Add(this.txtEventOrgId);
            this.tabSingleSport.Controls.Add(this.btnGetOrganizationEvents);
            this.tabSingleSport.Location = new System.Drawing.Point(4, 22);
            this.tabSingleSport.Name = "tabSingleSport";
            this.tabSingleSport.Padding = new System.Windows.Forms.Padding(3);
            this.tabSingleSport.Size = new System.Drawing.Size(669, 236);
            this.tabSingleSport.TabIndex = 8;
            this.tabSingleSport.Text = "SingleSport";
            this.tabSingleSport.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Event Id";
            // 
            // txtEventId
            // 
            this.txtEventId.Location = new System.Drawing.Point(6, 69);
            this.txtEventId.Name = "txtEventId";
            this.txtEventId.Size = new System.Drawing.Size(146, 20);
            this.txtEventId.TabIndex = 4;
            // 
            // btnGetEventResults
            // 
            this.btnGetEventResults.Location = new System.Drawing.Point(158, 66);
            this.btnGetEventResults.Name = "btnGetEventResults";
            this.btnGetEventResults.Size = new System.Drawing.Size(195, 23);
            this.btnGetEventResults.TabIndex = 3;
            this.btnGetEventResults.Text = "Get Event Results";
            this.btnGetEventResults.UseVisualStyleBackColor = true;
            this.btnGetEventResults.Click += new System.EventHandler(this.btnGetEventResults_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Organization Id";
            // 
            // txtEventOrgId
            // 
            this.txtEventOrgId.Location = new System.Drawing.Point(3, 23);
            this.txtEventOrgId.Name = "txtEventOrgId";
            this.txtEventOrgId.Size = new System.Drawing.Size(146, 20);
            this.txtEventOrgId.TabIndex = 1;
            // 
            // btnGetOrganizationEvents
            // 
            this.btnGetOrganizationEvents.Location = new System.Drawing.Point(155, 20);
            this.btnGetOrganizationEvents.Name = "btnGetOrganizationEvents";
            this.btnGetOrganizationEvents.Size = new System.Drawing.Size(195, 23);
            this.btnGetOrganizationEvents.TabIndex = 0;
            this.btnGetOrganizationEvents.Text = "Get Organization Events";
            this.btnGetOrganizationEvents.UseVisualStyleBackColor = true;
            this.btnGetOrganizationEvents.Click += new System.EventHandler(this.btnGetOrganizationEvents_Click);
            // 
            // tabMatchInsert
            // 
            this.tabMatchInsert.Controls.Add(this.label10);
            this.tabMatchInsert.Controls.Add(this.label9);
            this.tabMatchInsert.Controls.Add(this.label8);
            this.tabMatchInsert.Controls.Add(this.txtDeleteMatchId);
            this.tabMatchInsert.Controls.Add(this.btnDeleteMatchResult);
            this.tabMatchInsert.Controls.Add(this.txtInsertMatchId);
            this.tabMatchInsert.Controls.Add(this.txtInsertAwayTeamScore);
            this.tabMatchInsert.Controls.Add(this.btnInsertMatchResult);
            this.tabMatchInsert.Controls.Add(this.txtInsertHomeTeamScore);
            this.tabMatchInsert.Controls.Add(this.txtUpdateMatchId);
            this.tabMatchInsert.Controls.Add(this.txtUpdateAwayTeamScore);
            this.tabMatchInsert.Controls.Add(this.btnUpdateMatchResult);
            this.tabMatchInsert.Controls.Add(this.txtUpdateHomeTeamScore);
            this.tabMatchInsert.Location = new System.Drawing.Point(4, 22);
            this.tabMatchInsert.Name = "tabMatchInsert";
            this.tabMatchInsert.Padding = new System.Windows.Forms.Padding(3);
            this.tabMatchInsert.Size = new System.Drawing.Size(669, 236);
            this.tabMatchInsert.TabIndex = 9;
            this.tabMatchInsert.Text = "Insert";
            this.tabMatchInsert.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(114, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "HomeTeam Score";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(220, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Away Team Score";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "MatchId";
            // 
            // txtDeleteMatchId
            // 
            this.txtDeleteMatchId.Location = new System.Drawing.Point(11, 113);
            this.txtDeleteMatchId.Name = "txtDeleteMatchId";
            this.txtDeleteMatchId.Size = new System.Drawing.Size(100, 20);
            this.txtDeleteMatchId.TabIndex = 24;
            // 
            // btnDeleteMatchResult
            // 
            this.btnDeleteMatchResult.Location = new System.Drawing.Point(382, 110);
            this.btnDeleteMatchResult.Name = "btnDeleteMatchResult";
            this.btnDeleteMatchResult.Size = new System.Drawing.Size(153, 23);
            this.btnDeleteMatchResult.TabIndex = 23;
            this.btnDeleteMatchResult.Text = "Delete Match Result";
            this.btnDeleteMatchResult.UseVisualStyleBackColor = true;
            this.btnDeleteMatchResult.Click += new System.EventHandler(this.btnDeleteMatchResult_Click);
            // 
            // txtInsertMatchId
            // 
            this.txtInsertMatchId.Location = new System.Drawing.Point(11, 43);
            this.txtInsertMatchId.Name = "txtInsertMatchId";
            this.txtInsertMatchId.Size = new System.Drawing.Size(100, 20);
            this.txtInsertMatchId.TabIndex = 22;
            // 
            // txtInsertAwayTeamScore
            // 
            this.txtInsertAwayTeamScore.Location = new System.Drawing.Point(223, 43);
            this.txtInsertAwayTeamScore.Name = "txtInsertAwayTeamScore";
            this.txtInsertAwayTeamScore.Size = new System.Drawing.Size(100, 20);
            this.txtInsertAwayTeamScore.TabIndex = 21;
            // 
            // btnInsertMatchResult
            // 
            this.btnInsertMatchResult.Location = new System.Drawing.Point(382, 40);
            this.btnInsertMatchResult.Name = "btnInsertMatchResult";
            this.btnInsertMatchResult.Size = new System.Drawing.Size(153, 23);
            this.btnInsertMatchResult.TabIndex = 20;
            this.btnInsertMatchResult.Text = "Insert Match Result";
            this.btnInsertMatchResult.UseVisualStyleBackColor = true;
            this.btnInsertMatchResult.Click += new System.EventHandler(this.btnInsertMatchResult_Click_1);
            // 
            // txtInsertHomeTeamScore
            // 
            this.txtInsertHomeTeamScore.Location = new System.Drawing.Point(117, 43);
            this.txtInsertHomeTeamScore.Name = "txtInsertHomeTeamScore";
            this.txtInsertHomeTeamScore.Size = new System.Drawing.Size(100, 20);
            this.txtInsertHomeTeamScore.TabIndex = 19;
            // 
            // txtUpdateMatchId
            // 
            this.txtUpdateMatchId.Location = new System.Drawing.Point(11, 84);
            this.txtUpdateMatchId.Name = "txtUpdateMatchId";
            this.txtUpdateMatchId.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateMatchId.TabIndex = 18;
            // 
            // txtUpdateAwayTeamScore
            // 
            this.txtUpdateAwayTeamScore.Location = new System.Drawing.Point(223, 84);
            this.txtUpdateAwayTeamScore.Name = "txtUpdateAwayTeamScore";
            this.txtUpdateAwayTeamScore.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateAwayTeamScore.TabIndex = 17;
            // 
            // btnUpdateMatchResult
            // 
            this.btnUpdateMatchResult.Location = new System.Drawing.Point(382, 81);
            this.btnUpdateMatchResult.Name = "btnUpdateMatchResult";
            this.btnUpdateMatchResult.Size = new System.Drawing.Size(153, 23);
            this.btnUpdateMatchResult.TabIndex = 16;
            this.btnUpdateMatchResult.Text = "Update Match Result";
            this.btnUpdateMatchResult.UseVisualStyleBackColor = true;
            this.btnUpdateMatchResult.Click += new System.EventHandler(this.btnUpdateMatchResult_Click);
            // 
            // txtUpdateHomeTeamScore
            // 
            this.txtUpdateHomeTeamScore.Location = new System.Drawing.Point(117, 84);
            this.txtUpdateHomeTeamScore.Name = "txtUpdateHomeTeamScore";
            this.txtUpdateHomeTeamScore.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateHomeTeamScore.TabIndex = 15;
            // 
            // timerGetTodaysMatches
            // 
            this.timerGetTodaysMatches.Tick += new System.EventHandler(this.timerGetTodaysMatches_Tick);
            // 
            // btnGetClubInformation
            // 
            this.btnGetClubInformation.Location = new System.Drawing.Point(154, 119);
            this.btnGetClubInformation.Name = "btnGetClubInformation";
            this.btnGetClubInformation.Size = new System.Drawing.Size(139, 23);
            this.btnGetClubInformation.TabIndex = 24;
            this.btnGetClubInformation.Text = "Get Club Information";
            this.btnGetClubInformation.UseVisualStyleBackColor = true;
            this.btnGetClubInformation.Click += new System.EventHandler(this.btnGetClubInformation_Click);
            // 
            // ProdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 560);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtOutput);
            this.Name = "ProdForm";
            this.Text = "NIF Production Test Application";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProdForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabTournaments.ResumeLayout(false);
            this.tabTournaments.PerformLayout();
            this.tabOrganizations.ResumeLayout(false);
            this.tabOrganizations.PerformLayout();
            this.tabMatches.ResumeLayout(false);
            this.tabMatches.PerformLayout();
            this.tabEvents.ResumeLayout(false);
            this.tabEvents.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.tabResults.PerformLayout();
            this.tabThreadTest.ResumeLayout(false);
            this.tabRegions.ResumeLayout(false);
            this.tabFunctions.ResumeLayout(false);
            this.tabFunctions.PerformLayout();
            this.tabSingleSport.ResumeLayout(false);
            this.tabSingleSport.PerformLayout();
            this.tabMatchInsert.ResumeLayout(false);
            this.tabMatchInsert.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabTournaments;
        private System.Windows.Forms.TextBox txtTournamentOrgId;
        private System.Windows.Forms.Button btn_GetAllTournaments;
        private System.Windows.Forms.TabPage tabOrganizations;
        private System.Windows.Forms.TabPage tabMatches;
        private System.Windows.Forms.Button btn_GetTournamentMatches;
        private System.Windows.Forms.TextBox txtTournamentMatchId;
        private System.Windows.Forms.Button btnGetFederationDisciplines;
        private System.Windows.Forms.Button btnGetFederations;
        private System.Windows.Forms.Button btnGetSeasons;
        private System.Windows.Forms.TextBox txtTournamentSeasonId;
        private System.Windows.Forms.Button btnGetTournamentsBySeasonId;
        private System.Windows.Forms.TextBox txtMatchId;
        private System.Windows.Forms.Button btnGetMatchByMatchId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtTournamentEndDate;
        private System.Windows.Forms.TextBox txtTournamentStartDate;
        private System.Windows.Forms.Button btn_GetTournamentByDate;
        private System.Windows.Forms.TabPage tabEvents;
        private System.Windows.Forms.Button btnGetAllEvents;
        private System.Windows.Forms.TextBox txtEventsOrgId;
        private System.Windows.Forms.Button btnGetMatchTeams;
        private System.Windows.Forms.Button btnGetTournamentTable;
        private System.Windows.Forms.Button btnGetFederationClassCodes;
        private System.Windows.Forms.Button btnGetAllTournaments2;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtMatchesSeasonId;
        private System.Windows.Forms.Button btnGetTodaysMatches;
        private System.Windows.Forms.Timer timerGetTodaysMatches;
        private System.Windows.Forms.Button btnGetTeamsInformation;
        private System.Windows.Forms.TextBox txtTournamentId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetTournamentStanding;
        private System.Windows.Forms.TabPage tabThreadTest;
        private System.Windows.Forms.Button btnGetAllMatchesThreadTest;
        private System.Windows.Forms.TabPage tabRegions;
        private System.Windows.Forms.Button btnGetRegions;
        private System.Windows.Forms.Button btnThreadTest2;
        private System.Windows.Forms.Button btnGetFunctions;
        private System.Windows.Forms.TextBox txtFunctionsOrgId;
        private System.Windows.Forms.TabPage tabFunctions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFunctions_OrgId;
        private System.Windows.Forms.Button btnGetFunctionsByOrgId;
        private System.Windows.Forms.TabPage tabSingleSport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEventId;
        private System.Windows.Forms.Button btnGetEventResults;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEventOrgId;
        private System.Windows.Forms.Button btnGetOrganizationEvents;
        private System.Windows.Forms.TabPage tabMatchInsert;
        private System.Windows.Forms.TextBox txtInsertMatchId;
        private System.Windows.Forms.TextBox txtInsertAwayTeamScore;
        private System.Windows.Forms.Button btnInsertMatchResult;
        private System.Windows.Forms.TextBox txtInsertHomeTeamScore;
        private System.Windows.Forms.TextBox txtUpdateMatchId;
        private System.Windows.Forms.TextBox txtUpdateAwayTeamScore;
        private System.Windows.Forms.Button btnUpdateMatchResult;
        private System.Windows.Forms.TextBox txtUpdateHomeTeamScore;
        private System.Windows.Forms.TextBox txtDeleteMatchId;
        private System.Windows.Forms.Button btnDeleteMatchResult;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOrgLocalCouncilId;
        private System.Windows.Forms.Button btnGetClubs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtOrgFederationId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtOrgSeasonId;
        private System.Windows.Forms.TextBox txtMunicipalityId;
        private System.Windows.Forms.Button btnGetTournamentByMunicipality;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSearchOrgOrClub;
        private System.Windows.Forms.Button btnGetClubInformation;

    }
}

