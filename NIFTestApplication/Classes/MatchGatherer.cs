﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using NIFTestApplication.NIFService4.PROD;

namespace NIFTestApplication.Classes
{
    class MatchGatherer
    {
        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["password"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["username"];

        public bool Done = false;

        public MatchGatherer(ManualResetEvent doneEvent)
        {
            _doneEvent = doneEvent;
        }

        private ManualResetEvent _doneEvent = new ManualResetEvent(true);

        public int SeasonId { get; set; }

        public int SportId { get; set; }
        
        public void GetMatches(Object threadContext)
        {
            
            var name = Thread.CurrentThread.Name;

            if (threadContext != null)
            {
                int threadIndex = (int) threadContext;
            }

            var client = new TournamentServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new SeasonIdRequest(SeasonId);
            var tournamentResponse = client.GetSeasonTournaments(request);

            if (!tournamentResponse.Success)
            {
                return;
            }
            foreach (var tournament in tournamentResponse.Tournaments)
            {
                string fileString = string.Empty;
                string matches = string.Empty;

                fileString = "Starting: " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                int tournamentId = 0;
            
                var matchRequest = new TournamentMatchRequest(DateTime.Today, DateTime.Today, tournament.TournamentId);

                var matchResponse = client.GetTournamentMatches(matchRequest);

                if (!matchResponse.Success)
                {
                    continue;
                }

                
                foreach (var match in matchResponse.TournamentMatch)
                {
                    matches += match.Hometeam + @" - " + match.Awayteam + Environment.NewLine;
                    
                }


                tournamentId = tournament.TournamentId;

                // Now we shall store the information
                fileString += "Ending: " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                List<string> matchesToSave = matches.Split(Convert.ToChar("\n")).ToList();
                if (matchesToSave.Count == 1)
                {
                    if (matchesToSave[0] == string.Empty)
                    {
                        continue;
                    }
                }
            
                List<string> linesToSave = fileString.Split(Convert.ToChar("\n")).ToList();
                
                linesToSave.AddRange(matchesToSave);

                var lines = linesToSave.ToArray();
                string fileName = DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2) +
                                    DateTime.Today.Day.ToString().PadLeft(2) + @"_" + name + @"_t" + tournamentId + @"_season" +
                                    SeasonId +
                                    "_sport" + SportId + ".txt";

                System.IO.File.WriteAllLines(@"C:\Utvikling\SportsDataService\Test\" + fileName, lines);
                
            }
            _doneEvent.Set();
        }
    }
}
