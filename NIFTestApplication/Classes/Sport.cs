﻿namespace NIFTestApplication.Classes
{
    class Sport
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int OrgId { get; set; }
    }
}
