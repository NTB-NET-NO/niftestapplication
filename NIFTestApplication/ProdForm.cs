﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormNIF.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The form nif.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using NIFTestApplication.Classes;
using NIFTestApplication.NIFService4.PROD;
using NIFTestApplication.Repositories;

namespace NIFTestApplication
{
    /// <summary>
    ///     The form nif.
    /// </summary>
    public partial class ProdForm : Form
    {
        
        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["password"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["username"];

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProdForm" /> class.
        /// </summary>
        public ProdForm()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     The form 1_ load.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private List<Season> GetSeasons(int orgId)
        {
            if (orgId == 0)
            {
                if (txtTournamentOrgId.Text.Trim() == string.Empty)
                {
                    txtOutput.Text = @"You must set Federation Id in text box";
                    return null;
                }
            }

            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return null;
            }
            
            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new SeasonRequest(orgId);
            var response = client.GetFederationSeasons(request);

            return !response.Success ? null : response.Season.ToList();
        }

        private void btn_GetAllTournaments_Click(object sender, EventArgs e)
        {
            if (txtTournamentOrgId.Text.Trim() == string.Empty)
            {
                txtOutput.Text = @"You must set Federation Id in text box";
                return;
            }

            var orgId = Convert.ToInt32(txtTournamentOrgId.Text);

            var client = new TournamentService1Client();
            if (client.ClientCredentials == null)
            {
                return;
            }
            
            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var seasons = GetSeasons(orgId);

            var activeSeason = new Season();
            try
            {
                activeSeason = (from f in seasons
                                where f.SeasonStatus == "active"
                                      ||
                                      (f.FromDate <= DateTime.Today.Date &&
                                       f.ToDate >= DateTime.Today.Date)
                                select f
                               ).Single();
            }
            catch (Exception)
            {
                var sportDataMapper = new SportDataMapper();
                int sportId = sportDataMapper.GetSportByOrgId(orgId).Id;

                var seasonDataMapper = new SeasonDataMapper();
                try
                {
                    activeSeason.SeasonId = seasonDataMapper.GetActiveSeasonBySportId(sportId).SeasonId;
                }
                catch (Exception)
                {
                    return;
                }
            }

            var seasonRequest = new SeasonIdRequest1
                {
                    SeasonId = activeSeason.SeasonId
                };

            var response = client.GetSeasonTournaments(seasonRequest);

            if (!response.Success)
            {
                return;
            }
            List<Tournament> tournaments = response.Tournaments.ToList();

            foreach (var tournament in tournaments)
            {
                txtOutput.Text += @"Id: " + tournament.TournamentId + @" " +
                                  tournament.TournamentName + Environment.NewLine;

            }



        }

        private void btn_GetTournamentMatches_Click(object sender, EventArgs e)
        {
            if (txtTournamentMatchId.Text.Trim() == string.Empty)
            {
                txtOutput.Text = @"You must set TournamentId Id in text box";
                return;
            }

            int tournamentId = Convert.ToInt32(txtTournamentMatchId.Text);

            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var subclient = new OrgServiceClient();
            if (subclient.ClientCredentials == null)
            {
                return;
            }

            subclient.ClientCredentials.UserName.UserName = _ntbUsername;
            subclient.ClientCredentials.UserName.Password = _ntbPassword;

            

            var request = new TournamentMatchRequest
                {
                    TournamentId = tournamentId
                };

            var response = client.GetTournamentMatches(request);

            if (!response.Success)
            {
                return;
            }

            
            List<TournamentMatchExtended> tournamentMatches = response.TournamentMatch.ToList().OrderBy(t => t.MatchDate).ToList();
            
            txtOutput.Text = string.Empty;
            var sb = new StringBuilder();
            foreach (var matchExtended in tournamentMatches)
            {
                
                 sb.Append(matchExtended.MatchId 
                    + @" " + matchExtended.MatchDate 
                    + @" " + matchExtended.Hometeam.Trim()
                    + @" (" + matchExtended.HometeamId + @") "
                    + @" " + matchExtended.Awayteam.Trim()
                    + @" (" + matchExtended.AwayteamId + @") "
                    + @", StatusCode: " + matchExtended.StatusCode
                    + @", MatchResult: " + matchExtended.MatchResult
                    + Environment.NewLine);

                var searchParams = new OrgSearchParams
                {
                    OrgId = matchExtended.HometeamId
                };

                var subrequest = new SearchOrgAdvancedRequest
                {
                    SearchParams = searchParams
                };
                var subresponse = subclient.SearchOrgAdvanced(subrequest);

                if (!subresponse.Success) continue;

                foreach (var club in subresponse.Clubs)
                {
                    sb.Append(club.OrgName + Environment.NewLine);
                    sb.Append(club.OrgID + Environment.NewLine);
                }
            }

            txtOutput.Text += sb.ToString();
        }

        private void MatchExtendedOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            txtOutput.Text += propertyChangedEventArgs.PropertyName;
            
        }

        private void btnGetFederations_Click(object sender, EventArgs e)
        {
            var client = new OrgServiceClient();
            var tclient = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }


            if (tclient.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            tclient.ClientCredentials.UserName.UserName = _ntbUsername;
            tclient.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new EmptyRequest();
            var response = client.GetFederations(request);

            if (!response.Success)
            {
                return;
            }

            List<Federation> federations = response.Federations.ToList();
            txtOutput.Text = string.Empty;

            var outputString = new StringBuilder();
            foreach (Federation federation in federations)
            {
                outputString.Append(@"OrgId: " + federation.OrgId + Environment.NewLine +
                    @"OrgName: " + federation.OrgName + Environment.NewLine +
                    @"ShortName: " + federation.ShortName + Environment.NewLine +
                    @"BranchCode: " + federation.BranchCode + Environment.NewLine +
                    @"BranchId: " + federation.BranchId + Environment.NewLine +
                    @"BranchName: " + federation.BranchName + Environment.NewLine +
                    @"Diciplines: "+ federation.Disciplines + Environment.NewLine +
                    @"HasIndividualResults: " + federation.HasIndividualResults + Environment.NewLine +
                    @"HasTeamResults" + federation.HasTeamResults + Environment.NewLine +
                    Environment.NewLine);

                
                var trequest = new FederationDisciplineRequest
                    {
                        OrgId = federation.OrgId
                    };
                var tresponse = tclient.GetFederationDisciplines(trequest);
                if (!tresponse.Success) continue;

                foreach (FederationDiscipline discipline in tresponse.FederationDiscipline)
                {
                    outputString.Append(@"ActivityName: " + discipline.ActivityName + Environment.NewLine +
                                      @"Code: " + discipline.ActivityCode + Environment.NewLine +
                                      @"Id: " + discipline.ActivityId + Environment.NewLine);

                }
            }

            txtOutput.Text += outputString;
        }

        private void btnGetSeasons_Click(object sender, EventArgs e)
        {
            int orgId = Convert.ToInt32(txtOrgFederationId.Text);
            List<Season> seasons = GetSeasons(orgId);
            txtOutput.Text = string.Empty;

            if (seasons == null)
            {
                txtOutput.Text = @"Ingen sesong informasjon for denne organisasjonen";
                return;
            }

            var sb = new StringBuilder();

            foreach (Season season in seasons)
            {
                sb.Append(season.SeasonId + "\t" + season.SeasonName + "\t" +
                          season.FromDate + "\t" + season.ToDate + "\t" +
                          season.SeasonStatus
                          + Environment.NewLine);
                sb.Append(@"ActivityId: " + season.ActivityId + Environment.NewLine);
                sb.Append(@"ActivityName: " + season.ActivityName + Environment.NewLine);
                sb.Append(@"Description: " + season.Description + Environment.NewLine);
            }

            txtOutput.Text = sb.ToString();
        }

        private void btnGetTournamentsBySeasonId_Click(object sender, EventArgs e)
        {
            txtOutput.Text = string.Empty;

            if (txtTournamentSeasonId.Text == string.Empty)
            {
                txtOutput.Text = @"You must insert a seasonId" + Environment.NewLine;
                return;
            }

            int seasonId = Convert.ToInt32(txtTournamentSeasonId.Text);
            
            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var seasonRequest = new SeasonIdRequest
            {
                SeasonId = seasonId
            };

            var response = client.GetSeasonTournaments(seasonRequest);

            if (!response.Success)
            {
                return;
            }
            var tournaments = response.Tournaments.ToList();

            var sb = new StringBuilder();
            foreach (var tournament in tournaments.Where(t => t.SeasonId == seasonId))
            {
                sb.Append(@"Id: " + tournament.TournamentId + @" " + tournament.TournamentName + Environment.NewLine);
                sb.Append("Description: " + tournament.Description + Environment.NewLine);
                sb.Append("StatusCode: " + tournament.StatusCode + Environment.NewLine);
                sb.Append("TournamentAvailabilityTypeId: " + tournament.TournamentAvailabilityTypeId + Environment.NewLine);
                
            }

            txtOutput.Text += sb.ToString();
        }

        private void btnGetMatchByMatchId_Click(object sender, EventArgs e)
        {
            int matchid = Convert.ToInt32(txtMatchId.Text);

            var client = new ResultTeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var matchIdRequest = new MatchIdRequest(matchid);
            var response = client.GetMatchInfo(matchIdRequest);

            if (!response.Success)
            {
                return;
            }

            MatchInfo matchInfo = response.MatchInfo;

            txtOutput.Text += @"TournamentId: " + matchInfo.TournamentId
                              + @" Tournament Name: " + matchInfo.TournamentName
                              + Environment.NewLine
                              + matchInfo.Hometeam
                              + @" - "
                              + matchInfo.Awayteam
                              + @" "
                              + matchInfo.HomeGoals + @"-" + matchInfo.AwayGoals
                              + Environment.NewLine
                              + @"VenueUnitName: " + matchInfo.VenueUnitName
                              + Environment.NewLine
                              + @"VenueUnitId : " + matchInfo.VenueUnitNo 
                              + @"Partial result: " + matchInfo.PartialResult
                              + Environment.NewLine
                              + @"HomeTeamId: " + matchInfo.HometeamId + Environment.NewLine
                              + @"AwayTeamId: " + matchInfo.AwayteamId + Environment.NewLine;

            // 319861 
            // MatchId: 4788176

            if (matchInfo.HometeamId == null) return;

            
            var subClient = new FunctionServiceClient();
            if (subClient.ClientCredentials == null)
            {
                return;
            }

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;

            var personClient = new PersonServiceClient();

            if (personClient.ClientCredentials == null)
            {
                return;
            }

            personClient.ClientCredentials.UserName.UserName = _ntbUsername;
            personClient.ClientCredentials.UserName.Password = _ntbPassword;
            FunctionByOrgRequest request = new FunctionByOrgRequest
            {
                OrgId = matchInfo.HometeamId.Value
            };

            var subResponse = subClient.GetFunctionsForOrganisation(request);

            if (!subResponse.Success)
            {
                return;
            }

            var functions = subResponse.Functions;

            foreach (var function in functions)
            {
                txtOutput.Text += @"FunctionTypeId: " + function.FunctionTypeId + @" - " + function.FunctionTypeName + Environment.NewLine;
                txtOutput.Text += function.FirstName + @" " + function.LastName + Environment.NewLine;
                txtOutput.Text += @"PersonId: " + function.PersonId + @" - " + Environment.NewLine;

                var personRequest = new PersonIdRequest2
                    {
                        Id = function.PersonId
                    };

                var personIdResponse = personClient.GetPerson(personRequest);
                if (personIdResponse.Success)
                {
                    var personTest = personIdResponse.Person;
                    txtOutput.Text += @"PersonId: " + personTest.PersonId + Environment.NewLine;

                    txtOutput.Text += @"PhoneHome: " + personTest.HomeAddress.PhoneHome + Environment.NewLine;
                    txtOutput.Text += @"PhoneMobile: " + personTest.HomeAddress.PhoneMobile + Environment.NewLine;
                    txtOutput.Text += @"PhoneWork: " + personTest.HomeAddress.PhoneWork + Environment.NewLine;

                    
                }

                var searchResponse = personClient.SearchPersonByOrgIdAndFunctionTypeIds(new OrgIdFunctionTypeIdsRequest
                    {
                        OrgId = matchInfo.HometeamId.Value,
                        FunctionTypeIds = new int[] {function.FunctionTypeId}
                        ,
                        MaxRows = null
                    });

                
                
                if (!searchResponse.Success)
                {
                    return;
                }

                var persons = searchResponse.PersonsAndFunctions;

                foreach (var person in persons)
                {
                    txtOutput.Text += @"PersonId: " + person.PersonInfo.PersonId + Environment.NewLine;
                    txtOutput.Text += @"PhoneHome: " + person.PersonInfo.HomeTelephone + Environment.NewLine;
                    txtOutput.Text += @"MobileTelephone: " + person.PersonInfo.MobileTelephone + Environment.NewLine;
                    txtOutput.Text += @"WorkTelephone: " + person.PersonInfo.WorkTelephone + Environment.NewLine;
                }
                
                
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int matchid = Convert.ToInt32(txtMatchId.Text);

            var client = new ResultTeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var matchIdRequest = new MatchIdRequest(matchid);
            var response = client.GetMatchInfo(matchIdRequest);

            if (!response.Success)
            {
                return;
            }

            MatchInfo matchInfo = response.MatchInfo;
            
            MatchIncidentsResponse matchIncidentResponse  = client.GetMatchIncidents(matchIdRequest);

            if (!matchIncidentResponse.Success)
            {
                return;
            }
            txtOutput.Text = string.Empty;

            if (!matchIncidentResponse.MatchIncident.Any())
            {
                txtOutput.Text = @"Kampen inneholder ingen matchincidents." + Environment.NewLine;
                return;
            }

            

            foreach (MatchIncidentExtended matchIncident in matchIncidentResponse.MatchIncident)
            {
                var props = typeof (MatchIncident).GetProperties();
                txtOutput.Text += @"FirstName: " + matchIncident.FirstName + @", LastName: " + matchIncident.LastName + Environment.NewLine
                    + @"IncidentType: " + matchIncident.IncidentType + @", IncidentTypeId: " + matchIncident.IncidentTypeId + Environment.NewLine
                    + @", Reason: " + matchIncident.Reason + Environment.NewLine
                    + @"Time: " + matchIncident.Time + @"Period: " + matchIncident.Period + Environment.NewLine
                    + @"Value: " + matchIncident.Value + @"Period: " + matchIncident.Period + Environment.NewLine 
                    
                    + Environment.NewLine;
            }
        }

        private void btn_GetTournamentByDate_Click(object sender, EventArgs e)
        {
            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            DateTime dateStart = DateTime.Today.Date;
            DateTime dateEnd = DateTime.Today.Date;

            var matchRequest = new TournamentMatchRequest
                {
                    FromDate = dateStart,
                    ToDate = dateEnd,
                    TournamentId = 1234
                };

            var response = client.GetTournamentMatches(matchRequest);

            if (!response.Success)
            {
                return;
            }

            foreach (var match in response.TournamentMatch)
            {
                txtOutput.Text += match.MatchId + @": " +
                    match.Hometeam + @" - " + match.Awayteam + Environment.NewLine;
            }
        }

        private void btnGetAllEvents_Click(object sender, EventArgs e)
        {
            var orgId = Convert.ToInt32(txtEventsOrgId.Text);

            var client = new EventServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }
            

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;
            var request = new EventSearchRequest
                {
                    ActivityId = orgId
                };
            var response = client.SearchEvents(request);

            if (!response.Success) return;

            txtOutput.Text = string.Empty;
            foreach (EventSearchResult sportEvent in response.Events)
            {
                txtOutput.Text += sportEvent.EventId
                                  + "\t" + sportEvent.EventName
                                  + "\t" + sportEvent.StartDate
                                  + "\t" + sportEvent.Location
                                  + "\t" + sportEvent.RegionName
                                  + "\t" + sportEvent.ParentRegionName
                                  + "\t" + sportEvent.ParentActivityName
                                  + "\t" + sportEvent.ActivityName
                                  + "\t" + sportEvent.HasEntries
                                  + "\t" + sportEvent.HasResults
                                  + Environment.NewLine;
            }
        }

        private void btnGetMatchTeams_Click(object sender, EventArgs e)
        {
            int matchid = Convert.ToInt32(txtMatchId.Text);

            var client = new ResultTeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            
            var matchIdRequest = new MatchIdRequest(matchid);
            var response = client.GetMatchTeamPlayersByMatchId(matchIdRequest);

            if (!response.Success)
            {
                return;
            }

            if (!response.MatchTeamPlayer.Any())
            {
                txtOutput.Text = @"Vi finner ingen spillere knyttet til denne kampen." + Environment.NewLine;
            }

            foreach (MatchTeamPlayer matchTeamPlayer in response.MatchTeamPlayer)
            {
                txtOutput.Text += @"PlayerId: " + matchTeamPlayer.PlayerId + Environment.NewLine
                                  + matchTeamPlayer.Firstname + @" " + matchTeamPlayer.Lastname + Environment.NewLine
                                  + @"TeamId: " + matchTeamPlayer.TeamId + @"TeamName: " + matchTeamPlayer.TeamName + Environment.NewLine
                                  + @", ClubName: " + matchTeamPlayer.ClubName + @", ClubId: " + matchTeamPlayer.ClubId + Environment.NewLine
                                  + @"Kaptein: " + matchTeamPlayer.CaptainYN + Environment.NewLine
                                  + @"Debutant: " + matchTeamPlayer.DebutantYN + Environment.NewLine
                                  + Environment.NewLine; 
            }
        }

        private void btnGetTournamentTable_Click(object sender, EventArgs e)
        {
            if (txtTournamentMatchId.Text.Trim() == string.Empty)
            {
                return;
            }
            int tournamentId = Convert.ToInt32(txtTournamentMatchId.Text);

            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new TournamentIdRequest
                {
                    TournamentId = tournamentId
                };
            

            var response = client.GetTournamentTable(request);

            if (!response.Success)
            {
                txtOutput.Text = response.ErrorMessage + Environment.NewLine +
                                 response.ErrorCode + Environment.NewLine;
            }

            foreach (TournamentTable table in response.TournamentTable)
            {
                txtOutput.Text += table.TeamName + "\t"
                                  + table.NoMatches + "\t"
                                  + table.NoWins + "\t"
                                  + table.NoDraws + "\t"
                                  + table.NoLosses + "\t"
                                  + table.NoGoals + "\t"
                                  + table.NoAgainst + "\t"
                                  + table.Points + Environment.NewLine;
            }
            
        }

        private void btnGetFederationClassCodes_Click(object sender, EventArgs e)
        {
            int federationId = Convert.ToInt32(txtOrgFederationId.Text);

            var client = new MatchServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            var subClient = new ActivityServiceClient();
            if (subClient.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;

            var subRequest = new ActivitiesByOrgIdRequest
                {
                    Id = federationId
                };
            var subResponse = subClient.GetActivitiesByOrg(subRequest);

            if (!subResponse.Success)
            {
                return;
            }

            var genders = Enum.GetValues(typeof(Gender)).Cast<int>().ToList();
            foreach (Activity activity in subResponse.Activities)
            {
                txtOutput.Text += activity.Name + Environment.NewLine;
                foreach (var gender in genders)
                {
                    txtOutput.Text += gender + Environment.NewLine;
                    var request = new ClassCodesRequest
                        {
                            FederationId = federationId,
                            ActivityId = activity.Id,
                            Sex = gender
                        };
                    var response = client.GetClassCodes(request);

                    if (!response.Success) continue;
                    var classcodes = response.ClassCode;

                    foreach (ClassCode classCode in classcodes)
                    {
                        txtOutput.Text += classCode.ClassId + @", name: " + classCode.Name + Environment.NewLine
                                          + @"From Age: " + classCode.FromAge + @", To Age: " + classCode.ToAge +
                                          Environment.NewLine
                                          + @"Code: " + classCode.Code + Environment.NewLine;
                    }
                }
            }
        }

        private void btnGetAllTournaments2_Click(object sender, EventArgs e)
        {
            int federationId = Convert.ToInt32(txtTournamentOrgId.Text);

            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var searchParams = new TournamentTeamSearchParams
                {
                    FederationId = federationId
                };

            var request = new TournamentTeamSearchRequest(searchParams);
            var response = client.SearchTournamentByTeam(request);

            if (!response.Success)
            {
                return;
            }

            txtOutput.Text = string.Empty;
            foreach (Tournament tournament in response.Tournaments)
            {
                txtOutput.Text += @"classcode: " + tournament.ClassCodeId + Environment.NewLine
                             + @"Tournament Name: " + tournament.TournamentName + ", Id: " + tournament.TournamentId +
                             Environment.NewLine;
            }

        }

        private void btnGetTodaysMatches_Click(object sender, EventArgs e)
        {
            GetTodaysMatches();
            timerGetTodaysMatches.Interval = 120*1000;
            timerGetTodaysMatches.Enabled = true;
            timerGetTodaysMatches.Start();
        }

        private void GetTodaysMatches()
        {
            if (txtMatchesSeasonId.Text.Trim() == string.Empty)
            {
                txtOutput.Text = @"No season id provided." + Environment.NewLine;
            }

            var client = new TournamentServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            int seasonId = Convert.ToInt32(txtMatchesSeasonId.Text);

            // Now I want to get todays matches
            DateTime dateStart = DateTime.Today.Date;
            DateTime dateEnd = DateTime.Today.Date;

            var seasonRequest = new SeasonIdRequest
                {
                    SeasonId = seasonId
                };
            var tournamentResponse = client.GetSeasonTournaments(seasonRequest);

            if (!tournamentResponse.Success)
            {
                txtOutput.Text = tournamentResponse.ErrorMessage + Environment.NewLine;
            }

            
            foreach (Tournament tournament in tournamentResponse.Tournaments)
            {
                StringBuilder sb = new StringBuilder();
                var request = new TournamentMatchRequest
                    {
                        FromDate = dateStart,
                        ToDate = dateEnd,
                        TournamentId = tournament.TournamentId
                    };
                var matchResponse = client.GetTournamentMatches(request);

                if (!matchResponse.Success)
                {
                    continue;
                }

                if (!matchResponse.TournamentMatch.Any())
                {
                    continue;
                }

                string dateString = DateTime.Today.Date.ToShortDateString().Replace("-", "");
                string timeString = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
                string filename = @"C:\Utvikling\SportsDataService\Test\"
                                  + dateString + timeString + @"T"
                                  + tournament.TournamentId + ".txt";

                using (TextWriter writer = File.CreateText(filename))
                {
                    sb.Append(tournament.TournamentName + Environment.NewLine);
                    foreach (TournamentMatchExtended matchExtended in matchResponse.TournamentMatch)
                    {
                        sb.Append(matchExtended.MatchStartTime
                                  + @" " + matchExtended.MatchId
                                  + @" " + matchExtended.Hometeam
                                  + @" "
                                  + @"-"
                                  + @" " + matchExtended.Awayteam
                                  + @" " + matchExtended.StatusCode
                                  + @" " + matchExtended.MatchResult
                                  + @" " + matchExtended.HomeGoals
                                  + @" "
                                  + @"-"
                                  + @" " + matchExtended.AwayGoals
                                  + Environment.NewLine);
                        sb.Append(matchExtended.VenueUnitName + Environment.NewLine);
                    }

                    writer.Write(sb.ToString());
                    
                }

                sb.Clear();
                sb = null;                
            }


        }

        private void timerGetTodaysMatches_Tick(object sender, EventArgs e)
        {
            GetTodaysMatches();
        }

        private void btnGetFederationDisciplines_Click(object sender, EventArgs e)
        {
            if (txtOrgFederationId.Text.Trim() == string.Empty)
            {
                return;
            }

            int federationId = Convert.ToInt32(txtOrgFederationId.Text);

            var client = new TournamentServiceClient();
            var subClient = new OrgServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            if (subClient.ClientCredentials == null)
            {
                return;
            }

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;


            var request = new FederationDisciplineRequest
                {
                    OrgId = federationId
                };

            var response = client.GetFederationDisciplines(request);

            if (!response.Success)
            {
                txtOutput.Text = response.ErrorMessage + Environment.NewLine
                                 + response.ErrorCode;

                return;
            }

            foreach (FederationDiscipline discipline in response.FederationDiscipline)
            {
                txtOutput.Text += @"ActivityCode: " + discipline.ActivityCode + Environment.NewLine
                                  + @"ActivityId: " + discipline.ActivityId + Environment.NewLine
                                  + @"ActivityName: " + discipline.ActivityName + Environment.NewLine
                                  + Environment.NewLine;
            }

            // compare against the data in the federation object 
            var subRequest = new EmptyRequest();
            var subResponse = subClient.GetFederations(subRequest);

            if (!subResponse.Success)
            {
                txtOutput.Text += response.ErrorMessage + Environment.NewLine
                                 + response.ErrorCode;

                return;
            }

            Federation federation = (from r in subResponse.Federations
                              where r.OrgId == federationId
                              select r).Single();

            txtOutput.Text += federation.Disciplines;
        }

        private void btnGetClubInformation_Click(object sender, EventArgs e)
        {
            if (txtTournamentId.Text.Trim() == string.Empty)
            {
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += @"Ingen Tournament Id skrevet inn!";
                txtOutput.Text += Environment.NewLine;

                return;
            }

            var orgClient = new OrgServiceClient();

            var client = new TournamentServiceClient();
            var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            if (subClient.ClientCredentials == null)
            {
                return;
            }

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;

            if (orgClient.ClientCredentials == null)
            {
                return;

            }
            orgClient.ClientCredentials.UserName.UserName = _ntbUsername;
            orgClient.ClientCredentials.UserName.Password = _ntbPassword;


            int tournamentId = Convert.ToInt32(txtTournamentId.Text);

            var request = new TournamentIdRequest
            {
                TournamentId = tournamentId

            };
            var response = client.GetTournamentTeams(request);

            if (!response.Success)
            {
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += response.ErrorCode + @" : " + response.ErrorMessage;
                txtOutput.Text += Environment.NewLine;

                return;
            }

            foreach (TournamentMatchTeam matchTeam in response.TournamentMatchTeam)
            {
                // We shall now get the club for this team (if possible)
                

                txtOutput.Text += matchTeam.Team;
                txtOutput.Text += Environment.NewLine;

                var statsRequest = new TeamStatsRequest
                {
                    TeamId = matchTeam.TeamId
                };
                var statsResponse = subClient.GetTeamStats(statsRequest);

                if (statsResponse.Success) continue;
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += statsResponse.ErrorCode + @" : " + statsResponse.ErrorMessage;
                txtOutput.Text += Environment.NewLine;

                var orgRequest = new OrgIdRequest(matchTeam.TeamId);
                var orgResponse = orgClient.GetOrg(orgRequest);

                if (!orgResponse.Success)
                {
                    continue;
                }

                txtOutput.Text += orgResponse.Org.Name;
            }
        }

        private void btnGetTeamsInformation_Click(object sender, EventArgs e)
        {
            if (txtTournamentId.Text.Trim() == string.Empty)
            {
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += @"Ingen Tournament Id skrevet inn!";
                txtOutput.Text += Environment.NewLine;

                return;
            }

            var orgClient = new OrgServiceClient();

            var client = new TournamentServiceClient();
            var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            if (subClient.ClientCredentials == null)
            {
                return;
            }

            subClient.ClientCredentials.UserName.UserName = _ntbUsername;
            subClient.ClientCredentials.UserName.Password = _ntbPassword;

            if (orgClient.ClientCredentials == null)
            {
                return;

            }
            orgClient.ClientCredentials.UserName.UserName = _ntbUsername;
            orgClient.ClientCredentials.UserName.Password = _ntbPassword;

            
            
            int tournamentId = Convert.ToInt32(txtTournamentId.Text);

            var request = new TournamentIdRequest
                {
                    TournamentId = tournamentId

                };
            var response = client.GetTournamentTeams(request);

            if (!response.Success)
            {
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += response.ErrorCode + @" : " + response.ErrorMessage;
                txtOutput.Text += Environment.NewLine;

                return;
            }

            foreach (TournamentMatchTeam matchTeam in response.TournamentMatchTeam)
            {
                txtOutput.Text += matchTeam.Team;
                txtOutput.Text += Environment.NewLine;

                var statsRequest = new TeamStatsRequest
                    {
                        TeamId = matchTeam.TeamId
                    };
                var statsResponse = subClient.GetTeamStats(statsRequest);

                if (statsResponse.Success) continue;
                txtOutput.Text += Environment.NewLine;
                txtOutput.Text += statsResponse.ErrorCode + @" : " + statsResponse.ErrorMessage;
                txtOutput.Text += Environment.NewLine;
            }

        }

        private void btnGetTournamentStanding_Click(object sender, EventArgs e)
        {
            if (txtTournamentId.Text.Trim() == string.Empty)
            {
                txtOutput.Text = @"Ingen Tournament Id!";
            }

            int tournamentId = Convert.ToInt32(txtTournamentId.Text);

            var client = new TournamentServiceClient();
            // var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new TournamentIdRequest(tournamentId);
            var response = client.GetTournamentTable(request);

            if (!response.Success) return;
            foreach (var tournamentTable in response.TournamentTable)
            {
                txtOutput.Text += tournamentTable.TeamName + "\t" + tournamentTable.NoMatches
                                  + tournamentTable.NoWins + "\t"
                                  + tournamentTable.NoDraws + "\t"
                                  + tournamentTable.NoLosses + "\t"
                                  + tournamentTable.NoAgainst + "\t"
                                  + tournamentTable.NoGoals + "\t"
                                  + tournamentTable.Points;

                if (tournamentTable.SuddenDeathLosses != 0)
                {
                    txtOutput.Text += @"\t" + tournamentTable.SuddenDeathLosses;
                }

                if (tournamentTable.SuddenDeathWins != 0)
                {
                    txtOutput.Text += @"\t" + tournamentTable.SuddenDeathWins;
                }

                txtOutput.Text += Environment.NewLine;
            }
        }

        delegate void UpdateText(string text);
        
        public void SetTextBoxText(string text)
        {
            if (txtOutput.InvokeRequired)
            {
                txtOutput.Invoke(new UpdateText(SetTextBoxText), new object[] {text});
            }
            else
            {
                txtOutput.Text += text;
            }
        }
        
        private void btnGetAllMatchesThreadTest_Click(object sender, EventArgs e)
        {
            // Now we are to get results from the different team sports
            /*
             * handball: 210
             * Ice hockey : 152
             * bandy : 151
             * floorball : 72
             * 
             */
            
            // First we have to get the seasons for each of the disciplines
            var sportDictionary = new Dictionary<int, int>
                {
                    // Handball
                    {210, 200708}, 
                    // Ice hockey
                    {152, 200694}, 
                    // Bandy
                    {151, 200704}, 
                    // Floorball
                    {72, 200705}
                };

            ManualResetEvent[] doneEvents = new ManualResetEvent[sportDictionary.Count];

            int counter = 0;
            foreach (KeyValuePair<int, int> kvp in sportDictionary)
            {
                doneEvents[counter] = new ManualResetEvent(false);

                var matchGatherer = new MatchGatherer(doneEvents[counter])
                    {
                        SportId = kvp.Key,
                        SeasonId = kvp.Value
                    };

                

                ThreadPool.QueueUserWorkItem(matchGatherer.GetMatches, counter);

                // matchThread.Join();
                counter++;

            }

            foreach (ManualResetEvent doneEvent in doneEvents)
            {
                doneEvent.WaitOne();
            }
            txtOutput.Text = @"All match gatherings are complete!";


        }

        private void btnGetRegions_Click(object sender, EventArgs e)
        {
            var client = new RegionService1Client();
            // var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var request = new EmptyRequest5();
            
            var result = client.GetAllRegions(request);

            var stringBuilder = new StringBuilder();

            if (!result.Success)
            {
                return;
            }


            foreach (var region in result.Regions)
            {

                stringBuilder.Append(region.RegionId + ": " + region.RegionName + Environment.NewLine);
            }

            txtOutput.Text = stringBuilder.ToString();
        }

        private void btnThreadTest2_Click(object sender, EventArgs e)
        {
            // Now we are to get results from the different team sports
            /*
             * handball: 210
             * Ice hockey : 152
             * bandy : 151
             * floorball : 72
             * 
             */
            
            // First we have to get the seasons for each of the disciplines
            var sportDictionary = new Dictionary<int, int>
                {
                    // Handball
                    {210, 200708}, 
                    // Ice hockey
                    {152, 200694}, 
                    // Bandy
                    {151, 200704}, 
                    // Floorball
                    {72, 200705}
                };

            ManualResetEvent[] doneEvents = new ManualResetEvent[sportDictionary.Count];

            int counter = 0;
            List<Thread> threads = new List<Thread>();
            foreach (KeyValuePair<int, int> kvp in sportDictionary)
            {
                doneEvents[counter] = new ManualResetEvent(false);

                var matchGatherer = new MatchGatherer(doneEvents[counter])
                    {
                        SportId = kvp.Key,
                        SeasonId = kvp.Value
                    };

                Thread matchThread = new Thread(matchGatherer.GetMatches);
                matchThread.Name = @"SportId_" + kvp.Key;
                matchThread.Start();

                threads.Add(matchThread);

                // matchThread.Join();
                counter++;

            }

            foreach (Thread thread in threads)
            {
                txtOutput.Text += thread.Name + @": " + thread.IsAlive.ToString() + Environment.NewLine;
            }

            foreach (ManualResetEvent doneEvent in doneEvents)
            {
                doneEvent.WaitOne();
            }
            txtOutput.Text += @"All match gatherings are complete!";
        }

        private void btnGetFunctions_Click(object sender, EventArgs e)
        {
            if (txtFunctionsOrgId.Text == string.Empty)
            {
                return;
            }

            var orgId = Convert.ToInt32(txtFunctionsOrgId.Text.Trim());

            var client = new FunctionServiceClient();
            // var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            FunctionByOrgRequest request = new FunctionByOrgRequest
                {
                    OrgId = orgId
                };
            var response = client.GetFunctionsForOrganisation(request);

            if (!response.Success)
            {
                return;
            }

            var functions = response.Functions;

            foreach (var function in functions)
            {
                txtOutput.Text += @"FunctionTypeId: " + function.FunctionTypeId + @" " + function.FunctionTypeName + Environment.NewLine;
                txtOutput.Text += function.FirstName + @" " + function.LastName + Environment.NewLine;
                txtOutput.Text += @"PersonId: " + function.PersonId + @" " + function.LastName + Environment.NewLine;
            }
        }

        private void btnGetFunctionsByOrgId_Click(object sender, EventArgs e)
        {
            if (txtFunctions_OrgId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Organization Id" + Environment.NewLine;
            }


            var orgId = Convert.ToInt32(txtFunctions_OrgId.Text.Trim());

            var client = new FunctionServiceClient();
            // var subClient = new TeamServiceClient();
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            FunctionByOrgRequest request = new FunctionByOrgRequest
            {
                OrgId = orgId
            };
            var response = client.GetFunctionsForOrganisation(request);

            if (!response.Success)
            {
                return;
            }

            var functions = response.Functions;

            foreach (var function in functions)
            {
                txtOutput.Text += @"FunctionTypeId: " + function.FunctionTypeId + @" " + function.FunctionTypeName + Environment.NewLine;
                txtOutput.Text += function.FirstName + @" " + function.LastName + Environment.NewLine;
                txtOutput.Text += @"PersonId: " + function.PersonId + @" " + function.LastName + Environment.NewLine;
            }


        }

        private void btnGetOrganizationEvents_Click(object sender, EventArgs e)
        {
            var contentString = new StringBuilder();

            if (txtEventOrgId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Organization Id" + Environment.NewLine;
                return;
            }


            var orgId = Convert.ToInt32(txtEventOrgId.Text.Trim());

            var client = new EventServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;


            var activityClient = new ActivityServiceClient();
            if (activityClient.ClientCredentials != null)
            {
                activityClient.ClientCredentials.UserName.UserName = _ntbUsername;
                activityClient.ClientCredentials.UserName.Password = _ntbPassword;
            }

            var activityRequest = new ActivitiesByOrgIdRequest(orgId);
            var activityResponse = activityClient.GetActivitiesByOrg(activityRequest);
            if (!activityResponse.Success)
            {
                txtOutput.Text += activityResponse.ErrorMessage + Environment.NewLine;
                return;
            }

            var activities = activityResponse.Activities;

            foreach (var activity in activities)
            {
                var startDate = DateTime.Today.AddDays(-180);
                var endDate = DateTime.Today.AddDays(180);
            
                //var advancedParams = new EventSearchAdvancedParam
                //{
                //    ActivityCode = activity.ActivityCode,
                //    ActivityId = activity.Id,
                //    AdmOrgId = orgId,
                //    StartDate = startDate,
                //    EndDate = endDate
                //};

                // var criteria = new EventSearchAdvancedParam();
                var criteria = new EventSearchAdvancedSearchCriteria
                {
                    // ActivityCode = activity.ActivityCode,
                    // ActivityId = activity.Id,
                    AdmOrgId = orgId,
                    StartDate = startDate,
                    EndDate = endDate
                };

                var request = new EventSearchAdvancedRequest(criteria);

                var response = client.SearchEventsAdvanced(request);
                
                

                if (response.Success)
                {
                    var eventSearchResult = response.Events;

                    foreach (var foundEvent in eventSearchResult)
                    {
                        
                        contentString.Append("StartDate: " + foundEvent.StartDate + @": "  + Environment.NewLine);
                        contentString.Append("ActivityName: " + foundEvent.ActivityName + Environment.NewLine);
                        contentString.Append("AdministrationOrganizationName: " + foundEvent.AdministrationOrganizationName + Environment.NewLine);
                        contentString.Append("CompetitionHasResultList: " + foundEvent.CompetitionHasResultList + Environment.NewLine);
                        contentString.Append("CompetitionHasStartList: " + foundEvent.CompetitionHasStartList + Environment.NewLine);
                        contentString.Append("CounsilId: " + foundEvent.CounsilId + Environment.NewLine);
                        contentString.Append("CountyName: " + foundEvent.CountyName + Environment.NewLine);
                        contentString.Append("EndCouncil: " + foundEvent.EndCouncil + Environment.NewLine);
                        contentString.Append("EventLevelName: " + foundEvent.EventLevelName + Environment.NewLine);
                        contentString.Append("EventId: " + foundEvent.EventId + Environment.NewLine);
                        contentString.Append("EventName: " + foundEvent.EventName + Environment.NewLine);
                        contentString.Append("EventStatus: " + foundEvent.EventStatus + Environment.NewLine);
                        contentString.Append("EventTypeName: " + foundEvent.EventTypeName + Environment.NewLine);
                        contentString.Append("OrganizerName: " + foundEvent.OrganizerName + Environment.NewLine);
                        contentString.Append("StartCouncil: " + foundEvent.StartCouncil + Environment.NewLine);
                        contentString.Append("VenueUnitName: " + foundEvent.VenueUnitName + Environment.NewLine);




                    }
                }

            }
            
            
            
            txtOutput.Text += contentString;
        }

        private void btnGetEventResults_Click(object sender, EventArgs e)
        {
            if (txtEventId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Event Id" + Environment.NewLine;
                return;
            }

            var eventId = Convert.ToInt32(txtEventId.Text.Trim());

            if (txtEventOrgId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the org id" + Environment.NewLine;
                return;
            }
            var federationId = Convert.ToInt32(txtEventOrgId.Text); 

            var client = new ResultServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var orgClient = new OrgServiceClient();
            if (orgClient.ClientCredentials == null)
            {
                return;
            }

            orgClient.ClientCredentials.UserName.UserName = _ntbUsername;
            orgClient.ClientCredentials.UserName.Password = _ntbPassword;

            
            var resultString = new StringBuilder();

            var request = new ResultByEventIdRequest
            {
                EventId = eventId,
                ClassExerciseId = null
            };


            var response = client.GetResultsByEventIdOrClassExerciseId(request);

            if (!response.Success)
            {
                txtOutput.Text = response.ErrorMessage + Environment.NewLine;
                return;
            }

            var results = response.Results;
            var filteredResult = from myResult in results
                group myResult by myResult.ClassExerciseId
                into newGroup
                orderby newGroup.Key
                select newGroup;
                
                // response.Results.SetValu GroupBy(x => x.ClassExerciseId);
            var listOfClasses = new Dictionary<int, string>();
            foreach (var groups in filteredResult)
            {
                var name = groups.First().ClassExerciseName;
                var id = groups.First().ClassExerciseId;
                resultString.Append(name + Environment.NewLine);
                listOfClasses.Add(id, name);


                // resultString.Append("ClassExerciseName: " + groups.SingleOrDefault(x => x.ClassExerciseId == groups.Key).ClassExerciseName + Environment.NewLine);
                // resultString.Append("ClassExerciseId: " + groups.SingleOrDefault(x => x.ClassExerciseId == groups.Key).ClassExerciseId + Environment.NewLine);

                foreach (var result in groups.Where(x => x.Rank != null))
                {
                    resultString.Append("Rank: " + result.Rank + Environment.NewLine
                        + "Firstname: " + result.FirstName + Environment.NewLine
                        + "Lastname: " + result.LastName + Environment.NewLine
                        + "Club: " + result.Club + Environment.NewLine
                        + "CompetitorId: " + result.CompetitorId + Environment.NewLine
                        + "RankingPoints: " + result.RankingPoints + Environment.NewLine
                        + "Nationality: " + result.Nationality + Environment.NewLine
                        + "Time: " + result.Time + Environment.NewLine
                        + "ResultId: " + result.ResultId + Environment.NewLine
                        + "ResultType: " + result.ResultType + Environment.NewLine
                        + "StatusComment: " + result.StatusComment + Environment.NewLine
                        + "StatusString: " + result.StatusString + Environment.NewLine
                        + "StatusString2: " + result.StatusString2 + Environment.NewLine);

                    var orgRequest = new OrgIdRequest
                    {
                        OrgId = result.ClubId
                    };

                    // var orgResponse = orgClient.GetOrg(orgRequest);
                    var orgResponse = orgClient.GetOrgAdvanced(orgRequest);
                    if (orgResponse.Success)
                    {
                        var org = orgResponse.Club;

                        resultString.Append("AddressField1: " + org.AddressField1 + Environment.NewLine);
                        resultString.Append("AddressField2: " + org.AddressField2 + Environment.NewLine);
                        resultString.Append("AddressField3: " + org.AddressField3 + Environment.NewLine);
                        resultString.Append("AddressField4: " + org.AddressField4 + Environment.NewLine);
                        resultString.Append("RegionName: " + org.RegionName + Environment.NewLine);
                        resultString.Append("OrgName: " + org.OrgName + Environment.NewLine);
                    }

                    foreach (var resultvalue in result.ResultValues)
                    {
                        resultString.Append("ResultId: " + resultvalue.ResultId + Environment.NewLine
                                            + "Rank: " + resultvalue.Rank + Environment.NewLine
                                            + "HeatNumber: " + resultvalue.HeatNumber + Environment.NewLine
                                            + "IntegerValue:" + resultvalue.IntegerValue + Environment.NewLine);
                    }

                    resultString.Append(Environment.NewLine);
                }
                
                
            }

            bool found = false;
            var exerciseClient = new ExerciseServiceClient();

            if (exerciseClient.ClientCredentials == null)
            {
                return;
            }

            exerciseClient.ClientCredentials.UserName.UserName = _ntbUsername;
            exerciseClient.ClientCredentials.UserName.Password = _ntbPassword;


            var exerciseByEventIdRequest = new ExerciseByEventIdRequest
            {
                EventId = eventId,
                FederationId = federationId,
                InlcudeEventSpecificExercises = false
            };
            var exerciseResponse = exerciseClient.GetExercisesByEventId(exerciseByEventIdRequest);

            if (!exerciseResponse.Success)
            {
                return;
            }
            foreach (var exercise in exerciseResponse.Exercises)
            {
                if (listOfClasses.ContainsValue(exercise.Name))
                {
                    resultString.Append("exercise.Name: " + exercise.Name + Environment.NewLine);
                    resultString.Append("exercise.Id: " + exercise.ExerciseId + Environment.NewLine);
                    found = true;
                }
            }

            if (found == false)
            {
                foreach (var classes in listOfClasses)
                {
                    resultString.Append("Key: " + classes.Key + ", value: " + classes.Value + Environment.NewLine);
                }
            }

            txtOutput.Text += resultString;
        }

        private void btnInsertMatchResult_Click(object sender, EventArgs e)
        {
            if (txtInsertMatchId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Match Id" + Environment.NewLine;
                return;
            }

            if (txtInsertHomeTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }

            if (txtInsertAwayTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }


            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;
            var matchresult = new MatchResult
            {
                HomeGoals = Convert.ToInt32(txtInsertHomeTeamScore.Text),
                AwayGoals = Convert.ToInt32(txtInsertAwayTeamScore.Text),
                MatchId = Convert.ToInt32(txtInsertMatchId.Text)
            };

            var request = new MatchResultRequest(matchresult);
            var response = client.InsertMatchResult(request);

            if (response.Success)
            {
                // good!
            }
        }

        private void btnUpdateMatchResult_Click(object sender, EventArgs e)
        {
            if (txtUpdateMatchId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Match Id" + Environment.NewLine;
                return;
            }

            if (txtUpdateHomeTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }

            if (txtUpdateAwayTeamScore.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Home Team Score Id" + Environment.NewLine;
                return;
            }


            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;
            var matchresult = new MatchResult
            {
                HomeGoals = Convert.ToInt32(txtUpdateHomeTeamScore.Text),
                AwayGoals = Convert.ToInt32(txtUpdateAwayTeamScore.Text),
                MatchId = Convert.ToInt32(txtUpdateMatchId.Text),
                StatusCode = "R"
            };

            var request = new MatchResultRequest(matchresult);
            var response = client.UpdateMatchResult(request);

            if (response.Success)
            {
                // good!
            }
        }

        private void btnDeleteMatchResult_Click(object sender, EventArgs e)
        {
            if (txtDeleteMatchId.Text == string.Empty)
            {
                txtOutput.Text = @"You must provide the Match Id" + Environment.NewLine;
                return;
            }

            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;
            
            // Deleting the match result
            var request = new MatchResultDeleteRequest
            {
                MatchId = Convert.ToInt32(txtDeleteMatchId.Text)
            };

            var response = client.DeleteMatchResult(request);

            if (response.Success)
            {
                // good
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnInsertMatchResult_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInsertMatchId.Text))
            {
                throw new NullReferenceException("txtInsertMatchId");
            }

            if (string.IsNullOrEmpty(txtInsertHomeTeamScore.Text))
            {
                throw new NullReferenceException("txtInsertHomeTeamScore");
            }

            if (string.IsNullOrEmpty(txtInsertAwayTeamScore.Text))
            {
                throw new NullReferenceException("txtInsertAwayTeamScore");
            }

            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var homeTeamScore = Convert.ToInt32(txtInsertHomeTeamScore.Text);
            var awayTeamScore = Convert.ToInt32(txtInsertAwayTeamScore.Text);
            var matchId = Convert.ToInt32(txtInsertMatchId.Text);

            // Insert result
            var matchResult = new MatchResult
            {
                HomeGoals = homeTeamScore,
                AwayGoals = awayTeamScore,
                MatchId = matchId,
                StatusCode = "R"
            };
            
            var request = new MatchResultRequest(matchResult);
            var response = client.InsertMatchResult(request);

            if (response.Success)
            {
                txtOutput.Text = @"Result inserted correctly!" + Environment.NewLine;
            }

        }

        private void btnGetClubs_Click(object sender, EventArgs e)
        {
            int? councilId = null;
            if (txtOrgLocalCouncilId.Text != string.Empty)
            {
                councilId = Convert.ToInt32(txtOrgLocalCouncilId.Text);
            }

            int? federationId = null;
            if (txtOrgFederationId.Text != string.Empty)
            {
                federationId = Convert.ToInt32(txtOrgFederationId.Text);
            }

            int? seasonId = null;
            if (txtOrgSeasonId.Text != string.Empty)
            {
                seasonId = Convert.ToInt32(txtOrgSeasonId.Text);
            }



            var client = new TeamService1Client();
            
            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            var tournamentClient = new TournamentServiceClient();

            if (tournamentClient.ClientCredentials == null)
            {
                return;
            }

            tournamentClient.ClientCredentials.UserName.UserName = _ntbUsername;
            tournamentClient.ClientCredentials.UserName.Password = _ntbPassword;

            
            int orgId = 0;
            if (federationId != null)
            {
                
            }

            
            var searchParams = new TeamSearchParams
            {
                LocalCouncilId = councilId,
                FederationId = federationId
            };

            var request = new TeamSearchRequest1
            {
                SearchParams = searchParams
            };
            TeamSearchResponse1 response = client.SearchTeam(request);

            if (!response.Success)
            {
                return;
            }

            var teamInfos = response.TeamInfo;

            var sb = new StringBuilder();

            // This did not work.

            // What if we get all the teams in the different tournaments for this sport and see if we can find anything
            var seasonRequest = new SeasonIdRequest
            {
                SeasonId = Convert.ToInt32(seasonId)
            };

            var tournamentResponse = tournamentClient.GetSeasonTournaments(seasonRequest);

            if (!tournamentResponse.Success)
            {
                return;
            }

            var seasonTournaments = tournamentResponse.Tournaments;
            foreach (var tournament in seasonTournaments)
            {
                var tournamentRequest = new TournamentIdRequest
                {
                    TournamentId = tournament.TournamentId
                };

                var teamsResponse = tournamentClient.GetTournamentTeams(tournamentRequest);

                if (!teamsResponse.Success)
                {
                    continue;
                }

                var teams = teamsResponse.TournamentMatchTeam;
                if (!teams.Any())
                {
                    continue;
                }

                // filter teams / teaminfo
                var filteredTeams = (from ti in teamInfos 
                                     from t in teams
                                     where ti.OrgId == t.TeamId
                                     select ti);
                foreach (var teamInfo in filteredTeams)
                {
                    sb.Append(@"OrgId: " + teamInfo.OrgId + @" : " + teamInfo.OrgName + @", RegionName: " + teamInfo.RegionName + Environment.NewLine);
                    sb.Append(tournament.TournamentName + Environment.NewLine);
                }
            }
            
            
            //foreach (TeamInfo ti in teamInfos)
            //{
            //    sb.Append(ti.OrgId + @" : " + ti.OrgName + @", " + ti.RegionName + Environment.NewLine);

            //    // Shall we test and see if we can find a tournament for the team also?
            //    var teamSearchParams = new TournamentTeamSearchParams
            //    {
            //        TeamId = ti.OrgId,
            //        SeasonId = seasonId
            //    }; 
            //    var searchRequest = new TournamentTeamSearchRequest
            //    {
            //        SearchParams = teamSearchParams
            //    };
            //    var searchResponse = tournamentClient.SearchTournamentByTeam(searchRequest);

            //    if (!searchResponse.Success)
            //    {
            //        continue;
            //    }

            //    var tournaments = searchResponse.Tournaments;

            //    var listOfTournaments = (from t in tournaments where t.SeasonName.Contains("Innebandy") select t).ToList();

            //    if (!listOfTournaments.Any())
            //    {
            //        continue;
            //    }

            //    foreach (var tournament in listOfTournaments)
            //    {
                    
            //        sb.Append("tournamentId: " + tournament.TournamentId + ", " + tournament.TournamentName +
            //                  Environment.NewLine);
            //    }
            //}

            

            txtOutput.Text = sb.ToString();
        }

        private void btnGetTournamentByMunicipality_Click(object sender, EventArgs e)
        {
            if (txtMunicipalityId.Text == string.Empty)
            {
                txtOutput.Text = @"You must set a region Id" + Environment.NewLine;
                return;
            }

            int? orgId = null;
            if (txtTournamentOrgId.Text != string.Empty)
            {
                 orgId = Convert.ToInt32(txtTournamentOrgId.Text);
            }

            int? seasonId = null;
            if (txtTournamentSeasonId.Text != string.Empty)
            {
                seasonId = Convert.ToInt32(txtTournamentSeasonId.Text);
            }

            var tournamentClient = new TournamentServiceClient();

            if (tournamentClient.ClientCredentials == null)
            {
                return;
            }

            tournamentClient.ClientCredentials.UserName.UserName = _ntbUsername;
            tournamentClient.ClientCredentials.UserName.Password = _ntbPassword;

            var searchParams = new TournamentRegionsSearchParams
            {
                FederationId = orgId,
                LocalCouncilIds = txtMunicipalityId.Text,
                SeasonId = seasonId
            };
            var request = new TournamentRegionsSearchRequest(searchParams);
            var response = tournamentClient.SearchTournamentByRegions(request);

            if (!response.Success)
            {
                txtOutput.Text = response.ErrorMessage + Environment.NewLine;
            }

            var sb = new StringBuilder();
            foreach (var tournament in response.Tournaments)
            {
                sb.Append(tournament.TournamentName + Environment.NewLine);
            }
        }

        private void btnSearchOrgOrClub_Click(object sender, EventArgs e)
        {
            var client = new OrgServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            client.ClientCredentials.UserName.UserName = _ntbUsername;
            client.ClientCredentials.UserName.Password = _ntbPassword;

            MessageBox.Show("Not implemented yet!");
        }

        private void ProdForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

    }
}